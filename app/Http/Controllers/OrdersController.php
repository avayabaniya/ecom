<?php

namespace App\Http\Controllers;


use App\Mail\OrderPlaced;
use App\Notifications\NewOrder;
use App\Order;
use App\Product;
use App\ProductsAttribute;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class OrdersController extends Controller
{
    public function confirmOrder($url = null){

        //echo "test";die;

        $user_session_email = Session::get('Email');
        //echo $user_session_email; die;

        $cartNum = DB::table('cart')->where('user_email', $user_session_email)->count();

        //echo $cartNum; die;
        $cartItems = DB::table('cart')->where('user_email', $user_session_email)->get();
        $orderType = DB::table('payment_types')->where('url', $url)->first();
        $userDetails = DB::table('users')->where('email', $user_session_email)->first();

        if ($orderType->payment_type != "offline") {
            echo "Feature Coming Soon";die;
        }

        foreach ($cartItems as $cartItem){

            $discount = $cartItem->discount;
            if (empty($cartItem->discount)){
                $discount = 0;
            }

            $order = new Order();
            $order->product_id = $cartItem->product_id;
            $order->order_id = $userDetails->id;
            $order->order_type = $orderType->payment_type;
            $order->size = $cartItem->size;
            $order->payment_type = $orderType->title;
            $order->quantity = $cartItem->quantity;
            $order->order_price = ($cartItem->price *  $cartItem->quantity)  - $discount;
            $order->user_email = $user_session_email;
            $order->order_status = 'pending';
            $order->save();

            DB::table('cart')->where('user_email', $user_session_email)->delete();
        }
        //User::find(1)->notify(new NewOrder);
        //$when = Carbon::now()->addSeconds(10);
        //$users = User::find(1);
        //User::find(1)->notify(new NewOrder);
        //Notification::send($users, new NewOrder());

        $user = Auth::user();
        //1. write Mail::to.... code
        //2. create new mail using php artisan
        //3.create view to email and return that in build function of the email class created
        //4.loginto mailtrap.io then change the mail credentials according to the info provided by mailtrap
        //5.change email in config/email to change sent by email address
        //6. pass value through constructor if required
        //7.create a markdown mail
        //publich the components using php artisan

        set_time_limit(60);
        Mail::to($user)->send(new OrderPlaced($user, $order));

        return redirect('/')->with('flash_message_success','Your order has been placed');
    }

    public function viewOrders(){

        //$orders = Order::orderBy('id', 'DESC')->get();
        $orders = Order::with('product', 'user')->orderBy('id', 'DESC')->get();
        //dd($orders);
        //$orders = json_decode(json_encode($orders));

        //$productOrders = Product::where('id', $orders->product_id)->get();



        //echo "<pre>"; print_r($orders[0]->product_id); die;

        auth()->user()->unreadNotifications->markAsRead();

        return view('admin.orders.view_orders')->with(compact('orders'));
    }

    public function editOrder(Request $request){

        //$status = $_GET['status'];
        //echo $status; die;
        $data = $request->all();
        //echo "<pre>"; print_r($data); die;
        $id = $data['order_id'];
        $status = $data['status'];

        Order::where('id', $id)->update([
            'order_status' => $status
        ]);


        return redirect('admin/view-orders')->with('flash_message_success', 'Status Updated');
    }

    public function trackOrders()
    {
        if (Auth::check()){
            $orders = Order::with('product','user')->where('order_id', Auth::user()->id)->get();
            return view('products.view_old_order')->with(compact('orders'));
        }else{
            echo "Please login";die;
        }
    }

    public function deleteOrder($id){

        //Adding to stock when product from cart is deleted
        $orderInfo = DB::table('orders')->where('id', $id)->first();
        $stock = ProductsAttribute::where('product_id', $orderInfo->product_id)
            ->where('size', $orderInfo->size)
            ->first();
        $stock->stock = $stock->stock + $orderInfo->quantity;
        $stock->save();

        DB::table('orders')->where('id', $id)->delete();



        return redirect()->back()->with('flash_message_success', 'Order has been Cancelled');
    }
}
