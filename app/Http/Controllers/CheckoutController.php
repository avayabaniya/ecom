<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller
{
    public function checkout() {
        $session_id = Session::get('session_id');
        $session_user_email = Session::get('Email');
        $cartCount = DB::table('cart')->where(['session_id' => $session_id])->count();
        $cartNum = DB::table('cart')->where(['user_email' => $session_user_email])->count();

        $paymentTypes = DB::table('payment_types')->get();
        //echo $cartNum; die;
        if (empty($session_user_email)){
            //die("test");
            return view('users.login_register')->with('flash_message_success','Please Login to CheckOut your order');
            //return redirect('login-register')
        }

        if ($cartCount == 0 && empty($session_user_email)) {
            return redirect()->back()->with('flash_message_error', 'Your Cart is Empty 1');
        }


        if ($cartNum == 0){
            return redirect()->back()->with('flash_message_error','Your Cart is empty');
        }



        if(empty(Auth::check())){
            //return redirect('login-register')->with('flash_message_success','Please Login to CheckOut your order');
            return redirect(route('user.login'))->with('flash_message_success','Please Login to CheckOut your order');
        }

        $relatedProducts = Product::with('attributes')->inRandomOrder()->get();

        return view('checkout.checkout')->with(compact('paymentTypes', 'relatedProducts'));
    }

    public function addPaymentType(Request $request) {

        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;

            if (empty($data['description'])){
                $data['description'] = '';
            }

            if ($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()){
                    //echo "success";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/images/backend_images/payment_types/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/payment_types/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/payment_types/small/'.$filename;

                    //Resize image
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                    $img = $filename;

                }
            }

            if (empty($data['image'])){
                $img = '';
            }

            DB::table('payment_types')->insert([
               ['title' => $data['title'],
                   'payment_type' => $data['payment_type'],
                   'description' => $data['description'],
                   'url' => $data['url'],
                   'image' =>$img,
                   'enable' => $data['status']
                   ]
            ]);

            return redirect()->back()->with('flash_message_success', 'Payment Type successfully added');
        }



        return view('admin.payment.add_payment_type');
    }

    public function viewPaymentTypes(){

        $paymentTypes = DB::table('payment_types')->orderBy('id', 'desc')->get();
        //echo "<pre>"; print_r($paymentTypes); die;

        return view('admin.payment.view_payment_types')->with(compact('paymentTypes'));

    }

    public function editPaymentType(Request $request, $id = null){
        //echo "<pre>"; print_r($id); die;

        if ($request->isMethod('post')){
            $data = $request->all();

            if (empty($data['status'])){
                $data['status'] = 0;
            }
            if (empty($data['description'])){
                $data['description'] = '';
            }

            //echo "<pre>"; print_r($data); die;

            $img = $data['current_image'];

            //echo "<pre>"; print_r($img); die;

            if ($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()){
                    //echo "success";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/images/backend_images/payment_types/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/payment_types/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/payment_types/small/'.$filename;

                    //Resize image
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                    $img = $filename;

                }
            }
            //echo "<pre>"; print_r($img); die;

            //echo "<pre>"; print_r($img); die;


            DB::table('payment_types')
                ->where('id', $id)
                ->update(['title' => $data['title'],
                    'payment_type' => $data['payment_type'],
                    'description'=> $data['description'],
                    'url' => $data['url'],
                    'image'=>$img,
                    'enable'=>$data['status']]);

            return redirect('admin/view-payment-types')->with('flash_message_success', 'Successfully updated');


            //echo "<pre>"; print_r($data); die;
        }
        $paymentTypeDetails = DB::table('payment_types')->where('id' , $id)->first();
        return view('admin.payment.edit_payment_type')->with(compact('paymentTypeDetails'));

    }

    public function deletePaymentTypeImage($id = null){

        //Get product image name
        $paymentImage = DB::table('payment_types')->where('id', $id) ->first();

        //Get Product Image Paths
        $large_image_path = 'public/images/backend_images/payment_types/large/';
        $medium_image_path = 'public/images/backend_images/payment_types/medium/';
        $small_image_path = 'public/images/backend_images/payment_types/small/';

        //Delete Large Image if exists in folder
        if (file_exists($large_image_path.$paymentImage->image)) {
            unlink($large_image_path.$paymentImage->image);
        }

        //Delete Medium Image if  exists in folder
        if (file_exists($medium_image_path.$paymentImage->image)) {
            unlink($medium_image_path.$paymentImage->image);
        }

        //Delete Small Image if  exists in folder
        if (file_exists($small_image_path.$paymentImage->image)) {
            unlink($small_image_path.$paymentImage->image);
        }
        //Delete Image from Products table
        DB::table('payment_types')->where('id', $id)->update(['image'=>'']);
        return redirect()->back()->with('flash_message_success','Payment Type Image has been deleted successfully');

    }

}
