<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        //In Ascending order (bt default)
        //$productsAll = Product::get();
        //In Descending order
        $productsAll = Product::orderBy('id','DESC')->where('status',1)->get();
        //In random order
        //$productsAll = Product::inRandomOrder()->get();

        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
       /* $categories = json_decode(json_encode($categories));
        echo "<pre>"; print_r($categories);die;*/

        $banners = Banner::where('status', '1')->get();


        $relatedProducts = Product::with('attributes')->inRandomOrder()->get();

        return view('index')->with(compact('productsAll','categories', 'banners','relatedProducts'));

    }
}
