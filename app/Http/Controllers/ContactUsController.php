<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function contactUs(){

        $contactInfo = DB::table('contact_info')->first();
        $socialMedias = DB::table('social_media')->get();

        return view('contact_us.contact_us')->with(compact('contactInfo', 'socialMedias'));
    }

    public function viewContactInfo(){

        $contactInfo = DB::table('contact_info')->first();
        //echo "<pre>"; print_r($contactInfo);die;

        return view('admin.contact_us.view_contact_info')->with(compact('contactInfo'));
    }

    public function  editContactInfo(Request $request, $id = null){
        $contactInfo = DB::table('contact_info')->where('id', $id)->first();
        $countries = Country::get();

        if ($request->isMethod('post')) {

            $data = $request->all();
            //echo "<pre>"; print_r($request->all()); die;
            DB::table('contact_info')->where('id', $id)->update([
                'title' => $data['title'],
                'country' => $data['edit_country'],
                'district' => $data['district'],
                'address' => $data['address'],
                'number' => $data['number'],
                'email' => $data['email']
            ]);

            return redirect()->back()->with('flash_message_success', 'Contact Info Updated Successfully');
        }




        return view('admin.contact_us.edit_contact_info')->with(compact('contactInfo', 'countries'));


    }

    public function addSocialMedia(Request $request){


        if ($request->isMethod('post')){

           /* $this->validate($request, [

                'social_media_title' => 'required',
                'social_media_link' => 'required|url'
            ]);*/

            $data = $request->all();
            //echo "<pre>"; print_r($request->all()); die;

            $socialMediaCount = DB::table('social_media')->where('icon', $data['social_media_icon'])->count();
            if ($socialMediaCount > 0) {
                return redirect()->back()->with('flash_message_error', 'Social Media already exists');
            }

            if (empty($data['status'])){
                $data['status'] = 0;
            }

            DB::table('social_media')->insert([
                'title'=> $data['social_media_title'],
                'icon'=> $data['social_media_icon'],
                'link'=> $data['social_media_link'],
                'status'=>$data['status']
            ]);

            return redirect()->back()->with('flash_message_success', 'Social Media added successfully');

        }

        return view('admin.contact_us.add_social_media');
    }

    public function viewSocialMedia(){

        $socialMedias = DB::table('social_media')->get();

        return view('admin.contact_us.view_social_media')->with(compact('socialMedias'));
    }

    public function editSocialMedia(Request $request, $id = null){

        $socialMediaInfo = DB::table('social_media')->where('id', $id)->first();

        if ($request->isMethod('post')){

            $data = $request->all();

            if (empty($data['status'])){
                $data['status'] = 0;
            }

            DB::table('social_media')->where('id', $id)->update([
                'title'=> $data['social_media_title'],
                'icon'=> $data['social_media_icon'],
                'link'=> $data['social_media_link'],
                'status'=>$data['status']
            ]);

            return redirect()->back()->with('flash_message_success', 'Social Media Updated Successfully');
        }

        return view('admin.contact_us.edit_social_media')->with(compact('socialMediaInfo'));
    }

    public function deleteSocialMedia($id){

        DB::table('social_media')->where('id', $id)->delete();

        return redirect()->back()->with('flash_message_success', 'Social Media successfully deleted');
    }

    public function sendMail(Request $request){

         //$user = $request->all();
        //echo "<pre>"; print_r($request->all());die;

        $user['name'] = $request->input('name');
        $user['email'] = $request->input('email');
        $user['subject'] = $request->input('subject');
        $user['msg'] = $request->input('msg');
        // return $user['name'];

        mail::send('layouts.emailtemplate', $user, function($message) use ($user){

            $message->from($user['email']);

            $message->to('baniyaavaya2@gmail.com');

            $message->subject($user['subject']);

        });

        return redirect()->back()->with('flash_message_success','Your Mail has been send Successfully');

    }
}
