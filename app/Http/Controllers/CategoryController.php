<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;


class CategoryController extends Controller
{
    public function addCategory(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data);die;

            if (empty($data['status'])){
                $status = 0;
            }else{
                $status = 1;
            }

            if (empty($data['description'])){
                $data['description'] = '';
            }


            $category = new Category;
            $category->name =$data ['category_name'];
            $category->parent_id =$data['parent_id'];
            $category->description = $data['description'];
            $category->url =$data['url'];
            $category->status = $status;
            $category->save();

            //redirect to view category after adding with messages
            return redirect('/admin/view-categories')->with('flash_message_success', 'Category added Successfully' );

        }

        $levels = Category::where(['parent_id'=>0])->get();
       return view('admin.categories.add_category')->with(compact('levels'));
    }

    public function editCategory(Request $request, $id = null ) {
        if ($request->isMethod('post')) {
            $data = $request->all();
            /*echo "<pre>";print_r($data);die;*/

            $status = 0;
            if (!empty($data['status'])){
                $status = 1;
            }
            if (empty($data['description'])){
                $data['description'] = '';
            }
            Category::where(['id'=>$id])->update(
                ['name'=>$data['category_name'],
                    'description'=>$data['description'],
                    'url'=>$data['url'],
                    'status'=>$status]);
            return redirect('/admin/view-categories')->with('flash_message_success', 'Category updated Successfully!!');
        }
        $categoryDetails= Category::where(['id'=> $id])->first();
        $levels = Category::where(['parent_id'=>0])->get();
        return view('admin.categories.edit_category')->with(compact('categoryDetails', 'levels'));
    }

    public function deleteCategory($id = null){
        $productCount = Product::where('category_id', $id)->count();

        //echo "<pre>"; print_r($productCount); die;
        if ($productCount > 0) {
            return redirect()->back()->with('flash_message_error', 'Some products belong to this Category found.Cannot delete category who have one or more products under it');
            //die;
        }
        //die;
        if (!empty($id)) {
            $cat = Category::findOrFail($id);
            //die(print_r($cat->categories()));
            $cat->categories()->delete();
            Category::findOrFail($id)->delete();
            //Category::where('category_id', $id)->delete();
            return redirect()->back()->with('flash_message_success', 'Category Successfully deleted!!');
        }

    }

    public function viewCategories() {
        $categories = Category::orderBy('id', 'desc')->get();
        //$parentCategory = Category::where('parent_id', '!0');

        /*$categories =json_decode(json_encode($categories));die;*/
        return view('admin.categories.view_categories')->with(compact('categories'));

    }
}
