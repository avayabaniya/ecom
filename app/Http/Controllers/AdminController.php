<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    /*public function __construct()
    {
        $this->middleware('guest:admin');
    }*/

    public function login(Request $request){
        $pageDetails = DB::table('page_settings')->first();
    	if($request->isMethod('post')) {
    		$data = $request->input();
    		//echo "<pre>"; print_r($data); die;
    		//if(Auth::guard('admin')->attempt(['email'=>$data['email'], 'password' => $data['password'], 'admin' => '1']))
    		if(Auth::guard('admin')->attempt(['email'=>$data['email'], 'password' => $data['password']]))
    		{
    			//echo "success";die;
                //Session::put('adminSession', $data['email']);
                //return redirect()->intended(redirect('admin/dashboard'));
                return redirect('admin/dashboard');

    		}
            else
            {
    			//echo "Failed";die;
                //abort(404);
                return redirect('/admin')->with('flash_message_error', 'Invalid username or password');
    		}
    	}
        if(Auth::guard('admin')->check()){
            return Redirect::to('admin/dashboard');
        }
    	return view('admin.admin_login')->with(compact('pageDetails'));
    }

    public function dashboard()
    {
        /*if(Session::has('adminSession')){
            //perform all dashboard task
        }else{
            return redirect('/admin')->with('flash_message_error', 'Please login to access');
        }*/

        $totalUsers = User::all()->count();
        $totalProducts = Product::all()->count();
        $totalOrders = Order::all()->count();
        $pendingOrders = Order::where('order_status', 'pending')->count();



        return view ('admin.dashboard')->with(compact('totalUsers','totalProducts', 'totalOrders', 'pendingOrders'));
    }

    public function settings(){
        return view('admin.settings');
    }

    public function chkPassword(Request $request){
        $data = $request->all();
        $current_password = $data['current_pwd'];
        $check_password = User::where(['admin'=>'1'])->first();
        if(Hash::check($current_password,$check_password->password)){
            echo "true"; die;
        }else {
            echo "false"; die;
        }
    }

    public function updatePassword(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $check_password = Admin::where(['email' => Auth::user('admin')->email])->first();
            //echo "<pre>"; print_r($check_password); die;
            $current_password = $data['current_pwd'];
            if(Hash::check($current_password,$check_password->password))
            {
                $password = bcrypt($data['new_pwd']);
                Admin::where('id','1')->update(['password'=>$password]);
                return redirect('/admin/settings')->with('flash_message_success','Password updated Successfully!');
            }
            else {
                return redirect('/admin/settings')->with('flash_message_error','Incorrect Current Password!');
            }
        }
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/admin')->with('flash_message_success', 'Logged out Successful');
    }
}
