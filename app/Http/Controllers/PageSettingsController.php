<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Image;

class PageSettingsController extends Controller
{
    public function viewPageSettings(){

        $pageDetails = DB::table('page_settings')->first();

        //echo "<pre>"; print_r($pageDetails); die;

        return view('admin.page_settings.view_page_settings')->with(compact('pageDetails'));
    }

    public function editPageSettings(Request $request){

        $pageDetails = DB::table('page_settings')->first();

        if ($request->isMethod('post')) {
            $data = $request->all();

            if ($request->isMethod('post')) {
                $data = $request->all();

                //Upload Image
                //$filename = $data['current_image'] = '';
                if ($request->hasFile('logo')) {
                    $image_tmp = Input::file('logo');
                    if ($image_tmp->isValid()) {
                        //echo "test";die;
                        $extension = $image_tmp->getClientOriginalExtension();
                        $filename = rand(111, 99999) . '.' . $extension;
                        $image_path = 'public/images/backend_images/logo/' . $filename;

                        //Resize Images
                        Image::make($image_tmp)->save($image_path);
                    }
                } else {
                    $filename = $data['current_image'];
                }

                if (empty($data['description'])){
                    $data['description'] = '';
                }

                DB::table('page_settings')->where(['id'=>1])->update([
                   'title' => $data['title'],
                    'description' => $data['description'],
                    'logo' => $filename
                ]);

                return redirect()->back()->with('flash_message_success', 'Page Details has been updates successfully!');

            }

        }

        return view('admin.page_settings.edit_page_settings')->with(compact('pageDetails'));
    }

    //Support and services
    //paymentMethods
    public function paymentMethods(){

        $paymentTypes = DB::table('payment_types')->orderBy('id', 'desc')->get();
        $relatedProducts = Product::with('attributes')->inRandomOrder()->get();

        return view('page_settings.terms_and_condition.payment_methods')->with(compact('paymentTypes', 'relatedProducts'));
    }

    //How to place order
    public function howToPlaceOrder(){

        $info = DB::table('how_to_place_order')->first();

        return view('page_settings.support_and_services.how_to_place_order')->with(compact('info'));
    }

    public function viewHowToPlaceOrder(){

        $info = DB::table('how_to_place_order')->first();

        return view('admin.page_settings.support_and_services.view_how_to_place_order')->with(compact('info'));
    }

    public function editHowToPlaceOrder(Request $request){
        $info = DB::table('how_to_place_order')->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('how_to_place_order')->where(['id'=> 1])->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.support_and_services.edit_how_to_place_order')->with(compact('info'));


    }

    //FAQ
    public function faq(){

        $info = DB::table('faq')->first();

        return view('page_settings.support_and_services.faq')->with(compact('info'));
    }

    public function viewFAQ(){

        $info = DB::table('faq')->first();

        return view('admin.page_settings.support_and_services.view_faq')->with(compact('info'));
    }

    public function editFAQ(Request $request){
        $info = DB::table('faq')->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('faq')->where(['id'=> 1])->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.support_and_services.edit_faq')->with(compact('info'));


    }


    //Terms and Condition
    //Terms of use
    public function termsOfUse(){
        $info = DB::table('terms_of_use')->first();

        return view('page_settings.terms_and_condition.terms_of_use')->with(compact('info'));
    }

    public function viewTermsOfUse(){
        $info = DB::table('terms_of_use')->first();

        return view('admin.page_settings.terms_and_condition.view_terms_of_use')->with(compact('info'));
    }

    public function editTermsOfUse(Request $request){
        $info = DB::table('terms_of_use')->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('terms_of_use')->where(['id'=> 1])->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.terms_and_condition.edit_terms_of_use')->with(compact('info'));


    }

    //Refund Policy
    public function refundPolicy(){
        $info = DB::table('refund_policy')->first();

        return view('page_settings.terms_and_condition.refund_policy')->with(compact('info'));
    }

    public function viewRefundPolicy(){
        $info = DB::table('refund_policy')->first();

        return view('admin.page_settings.terms_and_condition.view_refund_policy')->with(compact('info'));
    }

    public function editRefundPolicy(Request $request){
        $info = DB::table('refund_policy')->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('refund_policy')->where(['id'=> 1])->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.terms_and_condition.edit_refund_policy')->with(compact('info'));


    }

    //About Company
    //company information
    public function companyInformation(){
        $info = DB::table('company_information')->first();

        return view('page_settings.about_company.company_information')->with(compact('info'));
    }

    public function viewCompanyInformation(){
        $info = DB::table('company_information')->first();

        return view('admin.page_settings.about_company.view_company_information')->with(compact('info'));
    }

    public function editCompanyInformation(Request $request){
        $info = DB::table('company_information')->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('company_information')->where(['id'=> 1])->update([
                'description' => $request->description
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.about_company.edit_company_information')->with(compact('info'));


    }

    //careers
    public function careers(){
        $careers = DB::table('careers')->get();

        return view('page_settings.about_company.careers')->with(compact('careers'));
    }

    public function viewCareers(){
        $careers = DB::table('careers')->orderBy('id', 'desc')->get();

        return view('admin.page_settings.about_company.view_careers')->with(compact('careers'));
    }

    public function addCareer(Request $request){
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('careers')->insert([
                'title' => $request->title,
                'department' => $request->department,
                'type' => $request->job_type
            ]);

            return redirect()->back()->with('flash_message_success','Career added Successfully');
        }

        return view('admin.page_settings.about_company.add_career');


    }

    public function editCareer(Request $request, $id=null){
        $career = DB::table('careers')->where('id', $id)->first();
        if ($request->isMethod('post')){

            if (empty($request->description)){
                $request->description = '';
            }

            DB::table('careers')->where(['id'=> $id])->update([
                'title' => $request->title,
                'department' => $request->department,
                'type' => $request->job_type
            ]);

            return redirect()->back()->with('flash_message_success','Updated Successfully');
        }

        return view('admin.page_settings.about_company.edit_career')->with(compact('career'));


    }

    public function deleteCareer($id){
        DB::table('careers')->where(['id'=> $id])->delete();
        return redirect()->back()->with('flash_message_success', 'Career details have been deleted Successfully!');
    }

}

