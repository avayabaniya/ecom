<?php

namespace App\Http\Controllers;

use App\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{

    public function userLoginRegister(){
        /*if(!empty(Session::get('Email'))){
            return Redirect::to('/');
        }*/

        if(Auth::guard()->check()){
            return Redirect::to('/');
        }
        return view('users.login_register');
    }

    public function login(Request $request) {
        if ($request->isMethod('post')){

            $session_id = Session::get('session_id');
            $data = $request->all();
            //echo "<pre>"; print_r($session_id); die;
            if (Auth::attempt(['email' => $data['email'], 'password'=>$data['password']])){
                Session::put('Email', $data['email']);
                Session::put('frontSession', $data['email']);
                //checks if a new product is added to cart before login
                if (!empty($session_id)){
                    return redirect('/update-cart');
                }

                return redirect('/cart');
            }else{
                return redirect()->back()->with('flash_message_error', 'Invalid Username or Password');
            }
        }
    }

    public function register(Request $request){

        if ($request->isMethod('post')){
            $data = $request->all();
            $session_id = Session::get('session_id');
            //echo "<pre>"; print_r($data);

            $usersCount = User::where('email',$data['email'])->count();
            if ($usersCount>0){
                return redirect()->back()->with('flash_message_error', 'Email already exists!');
            }else{
                //echo "Success"; die;
                $user = new User;
                $user->name = $data['name'];
                $user->email = $data['email'];
                $user->mobile = $data['number'];
                $user->city = $data['city'];
                $user->address = $data['address'];
                $user->password = bcrypt($data['password']);
                $user->save();
                if (Auth::attempt(['email'=>$data['email'], 'password'=>$data['password']])){
                    Session::put('Email', $data['email']);

                    //checks if a new product is added to cart before login
                    if (!empty($session_id)){
                        return redirect('/update-cart');
                    }

                    return redirect('/cart');
                }
            }
        }
    }

    public function account(Request $request){
        $user_id = Auth::user()->id;
        $userDetails = User::find($user_id);
        $countries = Country::get();
        //echo "<pre>"; print_r($countries); die;

        if ($request->isMethod('post')){
            $data = $request->all();


            if (empty($data['name'])){
                return redirect()->back()->with('flash_message_error', 'Please enter your name to update your account details');
            }
            //echo "<pre>"; print_r($data); die;
            $user = User::find($user_id);
            $user->name = $data['name'];
            $user->address = $data['address'];
            $user->city = $data['city'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->save();

            return redirect()->back()->with('flash_message_success', 'Your account details has been updated ');


        }

        return view('users.account')->with(compact('countries', 'userDetails'));
    }

    public function chkUserPassword(Request $request){
        $data = $request->all();
        //echo "<pre>";print_r($data); die;
        $current_password = $data['current_pwd'];
        $user_id = Auth::User()->id;
        $check_password = User::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function updatePassword(Request $request){
        if($request->isMethod('post'))
        {
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $check_password = User::where(['email' => Auth::user()->email])->first();
            //echo "<pre>"; print_r($check_password); die;
            $current_password = $data['current_pwd'];
            if(Hash::check($current_password,$check_password->password))
            {
                $password = bcrypt($data['new_pwd']);
                User::where('email',Auth::user()->email)->update(['password'=>$password]);
                return redirect('/account')->with('flash_message_success','Password updated Successfully!');
            }
            else {
                return redirect('/account')->with('flash_message_error','Incorrect Current Password!');
            }
        }
    }

    public function logout(){
        Auth::guard()->logout();
        Session::forget('Email');
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        Session::forget('TotalDiscount');
        Session::forget('flash_message_success');
        Session::forget('flash_message_error');
        Session::forget('session_id');
        return redirect('/');
    }

    public function checkEmail(Request $request){

        $data = $request->all();

        $usersCount = User::where('email',$data['email'])->count();
        if ($usersCount>0){
            echo "false";
        }else{
            echo "true";die;
        }
    }


}
