<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Coupon;
use App\Product;
use App\ProductsAttribute;
use App\ProductsImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\VarDumper\Cloner\Data;

class ProductsController extends Controller
{
    public function addProduct(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->all();
            //echo "<pre>";print_r($data);die;
            if (empty($data['category_id'])){
                return redirect()->back()->with('flash_message_error', 'Under Category is missing');
            }
            $product = new Product();
            $product->category_id = $data['category_id'];
            $product->product_name = $data['product_name'];
            $product->product_code = $data['product_code'];
            $product->product_color = $data['product_color'];
            if (!empty($data['description'])){
                $product->description = $data['description'];
            }else{
                $product->description = '';
            }
            if (!empty($data['care'])){
                $product->care = $data['care'];
            }else{
                $product->care = '';
            }
            $product->price = $data['price'];

            //Upload Image
            if ($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/images/backend_images/products/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/products/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/products/small/'.$filename;

                    //Resize Images
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                    //Store image name in products table
                    $product->image = $filename;
                }
            }
            //Check status
            if (empty($data['status'])){
                $status = 0;
            }else {
                $status = 1;
            }
            $product->status = $status;
            $product->save();


            //return redirect()->back()->with('flash_message_success', 'Product has been added successfully!');
            return redirect('/admin/view-products')->with('flash_message_success', 'Product has been added successfully!');
        }

            //Category Dropdown start
           $categories = Category::where(['parent_id'=>0])->get();
           $categories_dropdown = "<option value='' selected disabled>Select</option>";

           foreach ($categories as $cat)
           {
                $categories_dropdown .= "<option value='".$cat->id."'>".$cat->name."</option>";
                $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
                foreach ($sub_categories as $sub_cat) {
                    $categories_dropdown .= "<option value='".$sub_cat->id."'>&nbsp;--&nbsp;".$sub_cat->name."</option>";
                }
           }


        return view('admin.products.add_product')->with(compact('categories_dropdown'));
    }

    public function editProduct(Request $request, $id = null)
    {
        $productDetails = Product::where(['id'=>$id])->first();

        if ($request->isMethod('post'))
        {
            $data = $request->all();

            //Upload Image
            //$filename = $data['current_image'] = '';
            if ($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'public/images/backend_images/products/large/' . $filename;
                    $medium_image_path = 'public/images/backend_images/products/medium/' . $filename;
                    $small_image_path = 'public/images/backend_images/products/small/' . $filename;

                    //Resize Images
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);
                }
            }else{
                $filename = $data['current_image'];
            }

            //Check if description is empty
            if(empty($data['description'])){
                $data['description'] ='';
            }

            if(empty($data['care'])){
                $data['care'] ='';
            }

            //Check status
            if (empty($data['status'])){
                $status = 0;
            }else {
                $status = 1;
            }


            //echo "<pre>"; print_r($data);die;
            Product::where(['id'=>$id])->update(
                ['category_id'=>$data['category_id'],
                    'product_name'=>$data['product_name'],
                    'product_code'=>$data['product_code'],
                    'product_color'=>$data['product_color'],
                    'description'=>$data['description'],
                    'care'=>$data['care'],
                    'price'=>$data['price'],
                    'image'=>$filename,
                    'status'=>$status]
            );

            return redirect()->back()->with('flash_message_success', 'Product Updated successfully');
        }

        //Category dropdown start
        $categories = Category::where(['parent_id'=>0])->get();
        $categories_dropdown = "<option value='' selected disabled>Select</option>";

        foreach ($categories as $cat)
        {
            //Check for main categories
            if ($cat->id == $productDetails->category_id)
            {
                $selected ="selected";
            }else {
                $selected="";
            }
            $categories_dropdown .= "<option value='".$cat->id."' ".$selected.">".$cat->name."</option>";
            $sub_categories = Category::where(['parent_id'=>$cat->id])->get();
            foreach ($sub_categories as $sub_cat)
            {
                //check for sub categories
                if ($sub_cat->id == $productDetails->category_id)
                {
                    $selected = "selected";
                }else {
                    $selected = "";
                }
                $categories_dropdown .= "<option value='".$sub_cat->id."' ".$selected.">&nbsp;--&nbsp;".$sub_cat->name."</option>";
            }
        }
        //Category dropdown ends

        return view('admin.products.edit_product')->with(compact('productDetails', 'categories_dropdown'));
    }

    public function viewProducts()
    {
        $products = Product::orderBy('id', 'DESC')->get();

        //dd($products);

        foreach ($products as $key => $val){
            $category_name =Category::where(['id'=>$val->category_id])->first();
            $products[$key]->category_name = $category_name->name;
        }
        //echo "<pre>"; print_r($products); die;

        return view('admin.products.view_products')->with(compact('products'));
    }

    public function deleteProduct($id=null)
    {
        Product::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success', 'Product has been deleted successfully');
    }

    public function deleteProductImage($id = null)
    {
        //Get product image name
        $productImage = Product::where(['id' => $id])->first();

        //Get Product Image Paths
        $large_image_path = 'public/images/backend_images/products/large/';
        $medium_image_path = 'public/images/backend_images/products/medium/';
        $small_image_path = 'public/images/backend_images/products/small/';


       if (file_exists($large_image_path.$productImage->image)) {
            unlink($large_image_path.$productImage->image);
        }

        //Delete Medium Image if  exists in folder
        if (file_exists($medium_image_path.$productImage->image)) {
            unlink($medium_image_path.$productImage->image);
        }

        //Delete Small Image if  exists in folder
        if (file_exists($small_image_path.$productImage->image)) {
            unlink($small_image_path.$productImage->image);
        }
        //Delete Image from Products table
        Product::where(['id'=>$id])->update(['image'=>'']);
        return redirect()->back()->with('flash_message_success','Product Image has been deleted successfully');
    }

    public function deleteAltImage($id = null)
    {
        //Get product image name
        $productImage = ProductsImage::where(['id' => $id])->first();

        //Get Product Image Paths
        $large_image_path = 'public/images/backend_images/products/large/';
        $medium_image_path = 'public/images/backend_images/products/medium/';
        $small_image_path = 'public/images/backend_images/products/small/';

        //Delete Large Image if  exists in folder
        if (file_exists($large_image_path.$productImage->image)) {
            unlink($large_image_path.$productImage->image);
        }

        //Delete Medium Image if not exists in folder
        if (file_exists($medium_image_path.$productImage->image)) {
            unlink($medium_image_path.$productImage->image);
        }

        //Delete Small Image if not exists in folder
        if (file_exists($small_image_path.$productImage->image)) {
            unlink($small_image_path.$productImage->image);
        }
        //Delete Image from Products table
        ProductsImage::where(['id'=>$id])->delete();

        return redirect()->back()->with('flash_message_success','Product Alternate Image(s) has been deleted successfully');
    }

    public function addAttributes(Request $request, $id = null)
    {
        $productDetails = Product::with('attributes')->where(['id'=>$id])->first();


        if ($request->isMethod('post')){

            $data = $request->all();
            //$data brings back the 3 arrays inside another array making it an associative array
            //echo "<pre>";print_r($data);die;

            foreach ($data['sku'] as $key => $val) {
                //$key == '0','1'... $val = actual values
                if (!empty($val)){
                    //Prevent duplicate SKU
                    $attrCountSKU = ProductsAttribute::where('sku',$val)->count();
                    if ($attrCountSKU > 0){
                        return redirect('admin/add-attributes/'.$id)->with('flash_message_error', 'SKU already exists! Please add another SKU.');
                    }

                    //echo "<pre>"; print_r($id);die;

                    //Prevent duplicate size
                    $attrCountSize = ProductsAttribute::where(['product_id' => $id])->where( ['size'=>$data['size'][$key]])->count();
                    // echo "<pre>"; print_r($attrCountSize);die;
                    if ($attrCountSize > 0){
                        return redirect('admin/add-attributes/'.$id)->with('flash_message_error', '"'.$data['size'][$key].'" size already exists for this product! Please add another Size.');
                    }


                    $attribute = new ProductsAttribute();
                    $attribute->product_id = $id;
                    $attribute->sku = $val;
                    $attribute->size = $data['size'][$key]; //array whose first index is 'size' and 2nd index is the value of $key
                    $attribute->price = $data['price'][$key]; //array whose first index is 'price' and 2nd index is the value of $key
                    $attribute->stock = $data['stock'][$key];
                    $attribute->save();
                }
                //In 1 loop the value of $key is same
                //So all the value having the same $key are store in one single object of ProductsAttribute() model with product_id
               // echo "<pre>";print_r($attribute);die;
            }
           /* $productDetails = json_decode(json_encode($productDetails));
            echo "<pre>";print_r($productDetails);die;*/
            return redirect('admin/add-attributes/'.$id)->with('flash_message_success','Product Attribues has been added successfully!');
        }


        return view('admin.products.add_attributes')->with(compact('productDetails'));
    }

    public  function  editAttributes(Request $request, $id = null){
        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            foreach ($data['idAttr'] as $key => $attr) {
                ProductsAttribute::where(['id'=>$data['idAttr'][$key]])->update([
                    'price'=>$data['price'][$key],
                    'stock'=>$data['stock'][$key]
                ]);
            }
        return redirect()->back()->with('flash_message_success', 'Products Attributes has been updates successfully!');

        }
    }

    public function addImages(Request $request, $id = null)
    {
        $productDetails = Product::with('attributes')->where(['id'=>$id])->first();
        /*echo "<pre>"; print_r($id); die;*/

        if ($request->isMethod('post')){
            //add images
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            if ($request->hasFile('image')){
                $files = $request->file('image');
                foreach ($files as $file){

                    /*echo "<pre>"; print_r($data); die;*/
                    //Upload Image after resizing
                    $image = new ProductsImage;
                    $extension = $file->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;

                    $large_image_path = 'public/images/backend_images/products/large/'.$filename;
                    $medium_image_path = 'public/images/backend_images/products/medium/'.$filename;
                    $small_image_path = 'public/images/backend_images/products/small/'.$filename;

                    //Resize Images
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(600,600)->save($medium_image_path);
                    Image::make($file)->resize(300,300)->save($small_image_path);
                    $image->image = $filename;
                    $image->product_id = $productDetails['id'];
                    $image->save();

                }

            }
         return redirect('admin/add-images/'.$id)->with('flash_message_success','Product Images has been added successfully');
        }

        $productsImages = ProductsImage::where(['product_id'=>$id])->get();
        /*echo "<pre>"; print_r($productsImages); die;*/


        return view('admin.products.add_images')->with(compact('productDetails','productsImages'));
    }

    public function deleteAttribute($id = null) {
        ProductsAttribute::where(['id' => $id])->delete();
        return redirect()->back()->with('flash_message_success', 'Attribute has been deleted successfully');
    }

    public function products($url = null){

        $banners = Banner::where('status', '1')->get();
        $countCategory = Category::where(['url'=>$url, 'status'=>"1"])->count();
        if ($countCategory == 0){
            abort(404);
        }
        $categories = Category::with('categories')->where(['parent_id'=>0])->get();

        $categoryDetails = Category::where(['url' => $url ])->first();
        //echo "<pre>";print_r($categoryDetails);die;
        if ($categoryDetails->parent_id == 0){
            //If url is main category url
            $subCategories = Category::where(['parent_id'=>$categoryDetails->id])->get();
            //echo "<pre>";print_r($subCategories);die;


            foreach ($subCategories as $subcat){
                $cat_ids[] = $subcat->id;
            }
            //echo print_r($cat_ids);die;

            //if catagory doesnot have any item
            if (empty($cat_ids[0])){
                $productsAll = null;
            }else{
                $productsAll = Product::whereIn('category_id' ,$cat_ids)->where('status',1)->get();
            }



        }else{
            //If url is sub category url
            $productsAll = Product::where(['category_id' => $categoryDetails->id])->where('status',1)->get();
        }





        return view('products.listing')->with(compact('categories','categoryDetails', 'productsAll', 'banners'));
    }

    public function product($id = null){

        //Show 404 page if product is disabled
        $productCount = Product::where(['id' => $id,'status'=>1 ])->count();
        if ($productCount == 0){
            abort(404);
        }

        //Get Product Details
        $productDetails = Product::with('attributes')->where('id', $id)->first();
        /*echo '<pre>'; print_r($productDetails);die;*/

        $relatedProducts = Product::where('id', '!=', $id)->where(['category_id'=>$productDetails->category_id])->get();
        /*echo "<pre>"; print_r($relatedProducts);die;*/
        /*foreach ($relatedProducts->chunk(3) as $chunk){
            foreach ($chunk as $item){
                echo $item; echo "<br>";
            }
            echo "<br>";
        }*/

        //Get all categories and sub categories
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();

        //Get Products Alternate Image
        $productAltImages = ProductsImage::where('product_id', $id)->get();
        /*$productAltImages = json_decode(json_encode($productAltImages));
        echo '<pre>'; print_r($productAltImages);die;*/

        //Check stock
        $total_stock = ProductsAttribute::where('product_id', $id)->sum('stock');



        return view('products.detail')->with(compact('productDetails', 'categories', 'productAltImages', 'total_stock', 'relatedProducts'));
    }

    public function getProductPrice(Request $request){
        $data = $request->all();
        /*echo "<pre>"; print_r($data); die;*/
        $proArr = explode("-", $data['idSize']);
        /*echo $proArr[0]; echo$proArr[1]; die;*/
        $proAttr = ProductsAttribute::where(['product_id' => $proArr[0], 'size' => $proArr[1]])->first();
        echo $proAttr->price;
        echo "#";
        echo $proAttr->stock;
    }

    public function  addtocart(Request $request){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $data = $request->all();

        //echo "<pre>"; print_r($data);die;


        //user email session
        $userEmail = Session::get('Email');
        if (empty($userEmail)){
            $data['user_email'] = '';
        }else{
            $data['user_email'] = $userEmail;
        }

        $session_id = Session::get('session_id');
        if (empty($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }

        $sizeArr = explode("-", $data['size']);

        //echo "<pre>"; print_r($data);die;

        //When no size is selected
        if (empty($sizeArr[0])){
            return redirect()->back()->with('flash_message_error', 'Please select your size');
            //$sizeArr[0] = '';
            //$sizeArr[1] = '';
        }

        //check stock quantity
        $checkStock = ProductsAttribute::where('product_id', $data['product_id'])
            ->where('size', $sizeArr[1])
            ->first();

        if ($checkStock->stock < $data['quantity']){
            return redirect()->back()->with('flash_message_error', 'Required product quantity not available in stock');
        }

        //Checking fot duplicate products
        $countProduct = DB::table('cart')->where([
            'product_id' => $data['product_id'],
            'product_color' => $data['product_color'],
            'size' => $sizeArr[1],
            'session_id' => $session_id
        ])->count();

        if ($countProduct > 0) {
            return redirect()->back()->with('flash_message_error', 'Product already exists in Cart!');
        } else {


            $getSKU = ProductsAttribute::select('sku')->where(['product_id'=>$data['product_id'], 'size'=>$sizeArr[1]])->first();
            /*$getSKU = json_decode(json_encode($getSKU));
            echo "<pre>"; print_r($getSKU);die;*/

            //Product code if no size is selected
            if (empty($getSKU)) {
                $pcode = $data['product_code'];
            } else {
                $pcode = $getSKU->sku;
            }

            DB::table('cart')->insert([
                'product_id' => $data['product_id'],
                'product_name'=>$data['product_name'],
                'product_code' => $pcode,
                'product_color' => $data['product_color'],
                'price' => $data['price'],
                'size' => $sizeArr[1],
                'quantity' => $data['quantity'],
                'user_email' => $data['user_email'],
                'session_id' => $session_id
            ]);

        }

        //Deduct 1 stock when added to cart
        $stock = ProductsAttribute::where('product_id', $data['product_id'])
            ->where('size', $sizeArr[1])
            ->first();

        $stock->stock = $stock->stock - $data['quantity'];
        $stock->save();

        return redirect(route('cart'))->with('flash_message_success', 'Product has been added in Cart!');
    }

    public function updateCart(){

        $session_user_email = Session::get('Email');
        $session_id = Session::get('session_id');

        $newCartDetails = DB::table('cart')->where('session_id', $session_id)->get();
        //$id = $cartDetails[0]->id;
//        echo "<pre>"; print_r($cartDetails[0]->id);
//        echo "<pre>"; print_r($session_id);
//        echo "<pre>"; print_r($session_user_email);die;
       // $oldCartDetails = DB::table('cart')->where('user_email', $session_user_email)->get();
        //echo "<pre>"; print_r($oldCartDetails);die;


       foreach ($newCartDetails as $cart){
                 //echo $cart->product_id;die;
           //Checking fot duplicate products
           $countProduct = DB::table('cart')
               ->where(['product_id' => $cart->product_id])
               ->where(['product_color' => $cart->product_color])
               ->where(['size' => $cart->size])
               ->where(['user_email' => $session_user_email])
               ->count();

           if ($countProduct > 0 ) {
               return redirect('/cart')->with('flash_message_error', 'The product you selected already exists in your cart');
           }

           DB::table('cart')->where('id', $cart->id)->update(['user_email' => $session_user_email]);
//            DB::table('cart')->update([
//                'user_email' => $session_user_email
//            ])->where(['session_id'=>$session_id]);
        }

        //DB::table('cart')->where('id', $id)->update(['user_email' => $session_user_email]);
        /*DB::table('cart')->update([
            'user_email' => $session_user_email
        ])->where(['id'=>$id]);*/

        return redirect('/cart');


    }

    public function cart(){

        $session_user_email = Session::get('Email');
        if (empty($session_user_email)){
            $session_id = Session::get('session_id');
            $userCart = DB::table('cart')->where(['session_id' => $session_id])->get();
            foreach ($userCart as $key => $product){
                $productDetails = Product::where('id', $product->product_id)->first();
                //Ask sirrrrrrrrrr
                $userCart[$key]->image = $productDetails->image;
            }
        }else {
            $userCart = DB::table('cart')->where(['user_email' => $session_user_email])->get();
            foreach ($userCart as $key => $product){
                $productDetails = Product::where('id', $product->product_id)->first();
                //Ask sirrrrrrrrrr
                $userCart[$key]->image = $productDetails->image;
            }
        }

        //echo "<pre>"; print_r($userCart);die;
        return view('products.cart')->with(compact('userCart'));
    }

    public  function deleteCartProduct($id = null){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        //Adding to stock when product from cart is deleted
        $cartInfo = DB::table('cart')->where('id', $id)->first();
        $stock = ProductsAttribute::where('product_id', $cartInfo->product_id)
            ->where('size', $cartInfo->size)
            ->first();
        $stock->stock = $stock->stock + $cartInfo->quantity;
        $stock->save();

        DB::table('cart')->where('id', $id)->delete();



        return redirect('cart')->with('flash_message_success', 'Product has been deleted from Cart!');
    }

    public function updateCartQuantity( $id = null, $quantity = null){

        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        DB::table('cart')->where('id', $id)->update(['discount' => 0 ]);

        //echo $quantity;die;

        //Get detail from cart table when the id matches
        $getCartDetails = DB::table('cart')->where('id', $id)->first();
        //Get details from productAttributes table when the product_code matches the product_code of the item in cart table
        $getAttributeStock = ProductsAttribute::where('sku', $getCartDetails->product_code)->first();
        //Get quantity from cart table then add 1 or -1 to it
        $updated_quantity = $getCartDetails->quantity + $quantity;

        if ($quantity > 0){
            if ($getAttributeStock->stock != 0){
                DB::table('cart')->where('id', $id)->increment('quantity', $quantity);

                //Deduct or add  stock when cart quantity updated
                $getAttributeStock->stock = $getAttributeStock->stock - ($quantity);
                $getAttributeStock->save();

                return redirect('cart')->with('flash_message_success', 'Product Quantity has been updated Successfully');
            }else{
                return redirect('cart')->with('flash_message_error', 'Required Product Quantity not available!');

            }
        }elseif ($quantity < 0)
        {
            if ($updated_quantity != 0){
                DB::table('cart')->where('id', $id)->increment('quantity', $quantity);

                //Deduct or add  stock when cart quantity updated
                $getAttributeStock->stock = $getAttributeStock->stock - ($quantity);
                $getAttributeStock->save();

                return redirect('cart')->with('flash_message_success', 'Product Quantity has been updated Successfully');
            }else{
                return redirect('cart')->with('flash_message_error', 'Required Product Quantity not available!');

            }
        }


    }

    public function applyCoupon(Request $request){

        //remove old coupon sessions
        /* $oldSession = Session::get('CouponCode');
         if(!empty($oldSession)){
             $request->session()->forget('CouponCode');
             $request->session()->forget('CouponAmount');
         }*/

        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        Session::forget('CartId');

        $data = $request->all();
       // echo "<pre>"; print_r($data);die;
        $couponCount = Coupon::where('coupon_code', $data['coupon_code'])->count();
        if ($couponCount == 0){
            return redirect()->back()->with('flash_message_error', 'This Coupon does not exits!');
        }else{
            //Will perform other checks like active/inactive, Expiry date..

            //Get Coupon Details
            $couponDetails = Coupon::where('coupon_code', $data['coupon_code'])->first();
            $couponDetails = json_decode(json_encode($couponDetails));
            //echo "<pre>"; print_r($couponDetails);die;
            //If coupon is Inactive
            if ($couponDetails->status == 0) {
                return redirect()->back()->with('flash_message_error', 'This coupon is not active!');
            }

            //If coupon is Expired
            $expiry_date = $couponDetails->expiry_date;
            $current_date = date('Y-m-d');

            if ($expiry_date < $current_date) {
                return redirect()->back()->with('flash_message_error', 'This coupon is expired');
            }

            //Coupon is valid

            //Get Cart Total Amount
            $session_id = Session::get('Email');
            $userCart = DB::table('cart')->where(['user_email' => $session_id])->get();

            $total_amount = 0;
            //if (empty($data['cart_id'])) {
                foreach ($userCart as $item) {
                    $total_amount = $total_amount + ($item->price * $item->quantity);
                }
            //}else{
                //Session::put('CartId', $data['cart_id']);
                //$item = DB::table('cart')->where(['id' => $data['cart_id']])->first();
                //$total_amount = ($item->price * $item->quantity);
            //}

            //Check if amount type is fixed or percentage
            if ($couponDetails->amount_type == "Fixed") {
                $couponAmount = $couponDetails->amount;
            }else{
                $couponAmount = $total_amount * ($couponDetails->amount/100);
            }

            //echo $couponAmount;die;

            //Add Coupon Code and Amount in Session
            Session::put('CouponAmount', $couponAmount);
            Session::put('CouponCode', $data['coupon_code']);

            //echo Session::get('CouponAmount');
            //echo Session::get('CouponCode');die;

            return redirect()->back()->with('flash_message_success', 'Coupon code successfully applied. You will receive your discount! ');

        }


    }

    public function applyCoupontoProduct(Request $request, $id = null){


        $discount = [];


        Session::forget('CouponAmount');
        Session::forget('CouponCode');

        $data = $request->all();
        // echo "<pre>"; print_r($data);die;
        $couponCount = Coupon::where('coupon_code', $data['coupon_code'])->count();
        if ($couponCount == 0){
            return redirect()->back()->with('flash_message_error', 'This Coupon does not exits!');
        }else {
            //Will perform other checks like active/inactive, Expiry date..

            //Get Coupon Details
            $couponDetails = Coupon::where('coupon_code', $data['coupon_code'])->first();
            $couponDetails = json_decode(json_encode($couponDetails));
            //echo "<pre>"; print_r($couponDetails);die;
            //If coupon is Inactive
            if ($couponDetails->status == 0) {
                return redirect()->back()->with('flash_message_error', 'This coupon is not active!');
            }

            //If coupon is Expired
            $expiry_date = $couponDetails->expiry_date;
            $current_date = date('Y-m-d');

            if ($expiry_date < $current_date) {
                return redirect()->back()->with('flash_message_error', 'This coupon is expired');
            }

            //Coupon is valid
            Session::put('CartId', $data['cart_id']);
            $item = DB::table('cart')->where(['id' => $data['cart_id']])->first();
            $total_amount = ($item->price * $item->quantity);

            if ($couponDetails->amount_type == "Fixed") {
                $couponAmount = $couponDetails->amount;
            }else{
                $couponAmount = $total_amount * ($couponDetails->amount/100);
            }

            //set current discount in cart table
            DB::table('cart')->where('id', $data['cart_id'])->update(['discount'=> $couponAmount]);
            $carts = DB::table('cart')->where('user_email' , Session::get('Email'))->get();
            $total_discount = 0;
            foreach ($carts as $cart){
                $total_discount = $total_discount + $cart->discount;
            }
           // echo "<pre>"; print_r($cartInfo); die;


            Session::put('CouponAmount', $couponAmount);
            Session::put('CouponCode', $data['coupon_code']);
            Session::put('TotalDiscount', $total_discount);

            return redirect('/cart')->with('flash_message_success', 'Coupon successfully updated. You will recieve your discount');

        }




    }

}
