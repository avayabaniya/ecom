<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function mainCategories(){
        $mainCategories = Category::where(['parent_id' => 0 ])->get();
        return $mainCategories;
    }

    public static function pendingOrders(){

        $pendingOrder = Order::where('order_status', 'pending')->count();
        return $pendingOrder;
    }

    public static function cartCount(){
        $cartCount = DB::table('cart')->where('user_email', Session::get('Email'))->count();

        if ($cartCount == 0){
            $cartCount = null;
            return $cartCount;
        }
        return $cartCount;
    }

    public static function orderCount(){
        if (Auth::check()){
            $orderCount = DB::table('orders')->where('order_id', Auth::user()->id)->count();
            if ($orderCount == 0) {
                $orderCount = null;
                return $orderCount;
            }
            return $orderCount;
        }else{
            $orderCount = null;
            return $orderCount;
        }
    }


    public static function socialMedia(){
        $socialMedias = DB::table('social_media')->get();

        return $socialMedias;
    }

    public static function pageDetails(){
        $pageDetails = DB::table('page_settings')->first();

        return $pageDetails;
    }

}
