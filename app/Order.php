<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /*public function orderProducts() {
        return $this->hasone('App\Product','product_id');
    }*/

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function user(){
        return $this->belongsTo('App\User', 'order_id');
    }
}

