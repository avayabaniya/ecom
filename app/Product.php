<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function attributes() {
        return $this->hasMany('App\ProductsAttribute','product_id');
    }

    public function order(){
        return $this->hasMany('App\Order');
    }

    /*public function orderProducts() {
      return $this->hasone('App\Product');
  }*/


}
