-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2018 at 08:08 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sample`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Avaya', 'baniyaavaya@gmail.com', '$2y$10$FoKR7s9Qnn.GoFalQqlVOefRgWIS8MRez4DPZeyu5cJ2vFQoD5dom', 'A4BHyoHkM7zK8eNn0xK176XqnY7hwj6V0VxCivRNzINtC2HpcclcUvMd3HpX', NULL, '2018-09-24 06:25:49');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, '54457.png', 'T-Shirts', 'products/t-shirts', 0, '2018-09-04 10:24:41', '2018-10-28 04:43:30'),
(2, '63864.png', 'New Banner1', 'newbanner1', 0, '2018-09-04 10:26:20', '2018-10-28 03:49:55'),
(3, '66084.png', 'Black Friday', 'blackfriday', 0, '2018-09-05 05:01:37', '2018-10-28 03:51:44'),
(4, '69828.png', 'B1', 'b1', 1, '2018-10-28 03:49:27', '2018-10-28 03:49:27'),
(5, '52765.jpg', 'B2', 'b2', 1, '2018-10-28 03:50:41', '2018-10-28 03:50:41'),
(6, '11349.jpg', 'B3', 'be', 1, '2018-10-28 03:50:56', '2018-10-28 03:50:56');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `department`, `type`, `created_at`, `updated_at`) VALUES
(1, 'PR1', 'Marketing', 'fulltime', NULL, NULL),
(2, 'JR Developer', 'Developers', 'fulltime', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` float DEFAULT '0',
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `product_id`, `product_name`, `product_code`, `product_color`, `size`, `discount`, `price`, `quantity`, `user_email`, `session_id`, `created_at`, `updated_at`) VALUES
(4, 20, 'Green T-shirt', 'GTS001-L', 'Green', 'Large', 0, 1600, 1, 'hari@gmail.com', 'ahR6C983sJPrsgkMNkdGg78desHmUCClrN5SwvOH', NULL, NULL),
(5, 31, 'Checked Shirt', 'CSS002-M', 'Blue', 'Medium', 300, 1200, 1, 'yeyur@gmail.com', 'hFFhN6VOh70VwL6plR6sTsTgeoyoirZAuoE8WgN5', NULL, NULL),
(6, 28, 'Adidas prediators', 'AD001-S', 'Red and Black', 'Small', 0, 4000, 1, 'yeyur@gmail.com', 'hFFhN6VOh70VwL6plR6sTsTgeoyoirZAuoE8WgN5', NULL, NULL),
(12, 28, 'Adidas prediators', 'AD001-S', 'Red and Black', 'Small', 400, 4000, 1, 'asd@gmail.com', 'rGKV0R6dXdA1h4B51QvCyOxVj96TpRNZVb8w78Ex', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `description`, `url`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(11, 0, 'T-Shirts', 'T-Shirts Category', 't-shirts', 1, NULL, '2018-08-19 00:55:17', '2018-08-21 06:30:10'),
(12, 0, 'Shoes', 'Shoes Category', 'shoes', 1, NULL, '2018-08-19 00:55:47', '2018-08-19 00:55:47'),
(14, 11, 'Casual t-shirt', 'casual t-shirt', 'casual-t-shirts', 1, NULL, '2018-08-19 01:48:39', '2018-08-19 01:48:39'),
(17, 12, 'Sport Shoes', 'shoes', 'sport-shoes', 1, NULL, '2018-08-22 03:36:54', '2018-08-22 03:37:13'),
(18, 12, 'Casual Shoes', 'shoes', 'casual-shoes', 1, NULL, '2018-08-22 03:40:48', '2018-08-26 05:46:44'),
(19, 11, 'Formal T-shirt', 'formal shirt', 'formal-t-shirts', 1, NULL, '2018-08-27 04:41:42', '2018-08-27 04:42:04'),
(22, 12, 'Formal Shoes', 'Formal Shoes', 'formal-shoes', 1, NULL, '2018-08-31 03:51:40', '2018-08-31 03:51:40'),
(23, 0, 'Web Service', 'kjgkhlikh,klnlkhk', 'jbkjb', 1, NULL, '2018-10-26 04:06:48', '2018-10-26 04:06:48'),
(24, 23, 'Development', ',bjblkj', 'nlkhlkhi', 1, NULL, '2018-10-26 04:07:10', '2018-10-26 04:07:10'),
(25, 0, 'Electronics', 'aafdas', 'asdasd', 1, NULL, '2018-10-26 05:03:14', '2018-10-26 05:03:14'),
(26, 25, 'TV', 'tv', 'ttv', 1, NULL, '2018-10-26 05:04:23', '2018-10-26 05:04:23');

-- --------------------------------------------------------

--
-- Table structure for table `company_information`
--

CREATE TABLE `company_information` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_information`
--

INSERT INTO `company_information` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '<h3>OUR MISSION</h3>\r\n\r\n<p>If the internet can bring unprecedented convenience and speed to online shopping, then it can do the same for acts of community service. Daraz Cares is an initiative which aims to facilitate customers to help community in the most convenient way possible. We connect customers to charities 24/7; amplify empowering messages, and wish, in general, to encourage kindness and social concern. Daraz has partnered with multiple charities to bring convenience and ease to the donation process. Put some heart into your cart before you check-out with your shopping by donating to your preferred charities through Daraz Cares. Compassion is truly, just a click away.</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_info`
--

INSERT INTO `contact_info` (`id`, `title`, `country`, `district`, `address`, `number`, `email`, `created_at`, `updated_at`) VALUES
(1, 'ZealSale Online Market', 'Nepal', 'Kathmandu', 'Tinkune', '+977 9860123451', 'info@onlinezeal.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'SS', 'South Sudan'),
(203, 'ES', 'Spain'),
(204, 'LK', 'Sri Lanka'),
(205, 'SH', 'St. Helena'),
(206, 'PM', 'St. Pierre and Miquelon'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syrian Arab Republic'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania, United Republic of'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States minor outlying islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (U.S.)'),
(241, 'WF', 'Wallis and Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `amount_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `coupon_code`, `amount`, `amount_type`, `expiry_date`, `status`, `created_at`, `updated_at`) VALUES
(2, 'ctestper', 25, 'Percentage', '2018-09-30', 1, '2018-09-14 03:02:18', '2018-09-25 01:50:15'),
(3, 'avaya', 10, 'Percentage', '2018-10-31', 1, '2018-10-26 05:35:15', '2018-10-26 05:35:15');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>1) Do you Offer Discount?</strong></p>\r\n\r\n<p><strong>ans:&nbsp;</strong>Yes our website provides an coupon system through which you can get discount if u have the valid coupon code.</p>\r\n\r\n<hr />\r\n<p><strong>2) Do you offer online payment?</strong></p>\r\n\r\n<p><strong>ans:&nbsp;</strong>NO, not currently but we will be implementing it in the future. For now we provide cash on delivery system.</p>\r\n\r\n<p>&nbsp;</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `how_to_place_order`
--

CREATE TABLE `how_to_place_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `how_to_place_order`
--

INSERT INTO `how_to_place_order` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '<h3><strong>Shopping here&nbsp;is easy!</strong></h3>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>1</td>\r\n			<td>Click &lsquo;add to cart&rsquo;, to add this product to your cart,</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2</td>\r\n			<td>Click &lsquo;Go to next step&rsquo; in the top right corner,</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3</td>\r\n			<td>You will then need to fill in your contact details and preferred shipping address</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4</td>\r\n			<td>Choose &#39;Cash on Delivery&#39; payment option before clicking the &lsquo;Confirm order&rsquo; button</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewOrder\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":9:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:26:\\\"App\\\\Notifications\\\\NewOrder\\\":7:{s:2:\\\"id\\\";s:36:\\\"6b221754-f617-4e15-aafc-d48241cffaf9\\\";s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1537418643, 1537418643),
(2, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewOrder\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":9:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:26:\\\"App\\\\Notifications\\\\NewOrder\\\":7:{s:2:\\\"id\\\";s:36:\\\"6b221754-f617-4e15-aafc-d48241cffaf9\\\";s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1537418643, 1537418643),
(3, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewOrder\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":9:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:26:\\\"App\\\\Notifications\\\\NewOrder\\\":7:{s:2:\\\"id\\\";s:36:\\\"469bc526-db55-4988-ae75-aeb80aa4b3bc\\\";s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:8:\\\"database\\\";}s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1537514290, 1537514290),
(4, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewOrder\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":9:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:26:\\\"App\\\\Notifications\\\\NewOrder\\\":7:{s:2:\\\"id\\\";s:36:\\\"469bc526-db55-4988-ae75-aeb80aa4b3bc\\\";s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1537514290, 1537514290);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_17_054910_create_category_table', 2),
(4, '2018_08_19_090412_create_product_table', 3),
(5, '2018_08_19_090412_create_products_table', 4),
(6, '2018_08_22_101251_create_products_attributes_table', 5),
(7, '2018_08_31_103609_create_cart_table', 6),
(8, '2018_09_03_060210_create_coupons_table', 7),
(9, '2018_09_03_060846_create_coupons_table', 8),
(10, '2018_09_13_061850_create_orders_table', 9),
(11, '2018_09_14_101051_create_contact_info_table', 10),
(12, '2018_09_14_181153_create_social_media_table', 11),
(13, '2018_09_18_105620_create_jobs_table', 12),
(14, '2018_09_18_164117_create_notifications_table', 13),
(15, '2018_09_21_085401_create_admin_table', 14),
(16, '2018_09_24_203029_create_page_settings_table', 15),
(17, '2018_09_25_093545_create_how_to_place_order_table', 16),
(18, '2018_09_25_093749_create_terms_of_use_table', 16),
(19, '2018_09_25_093844_create_refund_policy_table', 16),
(20, '2018_09_25_093933_create_company_information_table', 16),
(21, '2018_09_25_214956_create_faq_table', 17),
(22, '2018_09_25_235137_create_careers_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('1e6ce765-ce17-4003-b467-afc70bdec2e7', 'App\\Notifications\\NewOrder', 'App\\User', 1, '{\"data\":\"New Order has been placed\"}', '2018-09-19 15:59:54', '2018-09-19 15:34:26', '2018-09-19 15:59:54'),
('7305e5eb-c9f1-4661-af71-14e04c43550f', 'App\\Notifications\\NewOrder', 'App\\User', 1, '{\"data\":\"New Order has been placed\"}', '2018-09-19 15:59:54', '2018-09-19 15:37:01', '2018-09-19 15:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_price` double(8,2) NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `order_type`, `payment_type`, `order_price`, `user_email`, `order_status`, `created_at`, `updated_at`) VALUES
(1, 27, 'offline', 'Cash on Delivery', 600.00, 'baniyaavaya2@gmail.com', 'completed', '2018-09-19 15:30:58', '2018-09-24 05:17:06'),
(2, 31, 'offline', 'Cash on Delivery', 900.00, 'baniyaavaya2@gmail.com', 'completed', '2018-09-19 15:30:58', '2018-09-20 04:07:33'),
(3, 21, 'offline', 'Cash on Delivery', 1000.00, 'hari@gmail.com', 'completed', '2018-09-19 15:36:59', '2018-09-24 05:17:03'),
(4, 31, 'offline', 'Cash on Delivery', 1300.00, 'baniyaavaya2@gmail.com', 'completed', '2018-09-19 22:59:02', '2018-10-26 04:38:07'),
(5, 31, 'offline', 'Cash on Delivery', 850.00, 'baniyaavaya2@gmail.com', 'pending', '2018-09-21 01:33:10', '2018-09-26 00:39:45'),
(6, 28, 'offline', 'Cash on Delivery', 3000.00, 'baniyaavaya2@gmail.com', 'pending', '2018-09-24 05:09:48', '2018-09-26 00:39:40'),
(7, 27, 'offline', 'Cash on Delivery', 900.00, 'baniyaavaya2@gmail.com', 'pending', '2018-09-24 05:09:48', '2018-09-24 05:09:48'),
(8, 27, 'offline', 'Cash on Delivery', 1000.00, 'baniyaavaya2@gmail.com', 'pending', '2018-09-24 05:14:02', '2018-09-24 05:14:02'),
(9, 31, 'offline', 'Cash on Delivery', 900.00, 'baniyaavaya2@gmail.com', 'pending', '2018-09-24 05:15:07', '2018-09-24 05:15:07'),
(10, 27, 'offline', 'Cash on Delivery', 750.00, 'hari@gmail.com', 'on-way', '2018-09-24 23:04:21', '2018-09-26 00:26:16'),
(11, 31, 'offline', 'Cash on Delivery', 900.00, 'yeyur@gmail.com', 'pending', '2018-09-26 00:24:29', '2018-09-26 00:24:29'),
(12, 31, 'offline', 'Cash on Delivery', 1275.00, 'yeyur@gmail.com', 'completed', '2018-09-26 00:24:29', '2018-09-26 00:24:54'),
(13, 20, 'offline', 'Cash on Delivery', 1400.00, 'hari@gmail.com', 'on-way', '2018-09-26 00:25:39', '2018-09-26 00:26:06'),
(14, 31, 'offline', 'Cash on Delivery', 1200.00, 'baniyaavaya2@gmail.com', 'pending', '2018-10-26 02:49:00', '2018-10-26 02:49:00'),
(15, 28, 'offline', 'Cash on Delivery', 4000.00, 'baniyaavaya2@gmail.com', 'pending', '2018-10-26 04:37:43', '2018-10-26 04:37:43'),
(16, 28, 'offline', 'Cash on Delivery', 4100.00, 'baniyaavaya2@gmail.com', 'pending', '2018-10-26 04:57:41', '2018-10-26 04:57:41'),
(17, 28, 'offline', 'Cash on Delivery', 4000.00, 'baniyaavaya2@gmail.com', 'pending', '2018-10-26 04:57:41', '2018-10-26 04:57:41'),
(18, 36, 'offline', 'Cash on Delivery', 1200.00, 'asd@gmail.com', 'pending', '2018-10-26 05:14:48', '2018-10-26 05:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `page_settings`
--

CREATE TABLE `page_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_settings`
--

INSERT INTO `page_settings` (`id`, `title`, `description`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'E-Shopper1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor 3', '66782.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`id`, `title`, `payment_type`, `description`, `url`, `image`, `enable`) VALUES
(1, 'ESewa', 'online', 'Secure way of online payment.', 'esewa', '63549.jpg', 1),
(2, 'PayPal', 'online', 'Pay online internationally', 'paypal', '79899.png', 0),
(3, 'Cash on Delivery', 'offline', 'Pay cash on delivery', 'cash-on-delivery', '19304.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `care` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `product_code`, `product_color`, `description`, `care`, `price`, `image`, `status`, `created_at`, `updated_at`) VALUES
(20, 14, 'Green T-shirt', 'CS001', 'Green', 'green', '', 1000.00, '29160.jpg', 1, '2018-08-22 01:08:17', '2018-08-31 00:26:24'),
(21, 14, 'Casual Tshirt', 'CS002', 'Blue', 'blue', '', 100.00, '97506.jpg', 1, '2018-08-22 01:08:55', '2018-08-25 23:00:35'),
(27, 18, 'Converse', 'V10', 'black', '', '', 1000.00, '89426.jpg', 1, '2018-08-22 03:30:04', '2018-09-26 00:52:20'),
(28, 17, 'Adidas prediators', 'AD101', 'Red and Black', 'Made for futsal', '!Build of Leather', 4000.00, '74039.jpg', 1, '2018-08-29 05:56:59', '2018-09-26 00:51:34'),
(29, 14, 'Vans Casual T-shirt', 'CT003', 'Black', 'Black T-Shirt', 'Cotton', 1000.00, '6157.jpg', 1, '2018-08-31 00:23:41', '2018-08-31 00:23:41'),
(31, 19, 'Checked Shirt', 'FT001', 'Blue', 'Checked Shirt', 'Cotton', 1300.00, '24325.jpg', 1, '2018-08-31 00:30:21', '2018-08-31 04:11:11'),
(33, 22, 'Brown Formal Shoe', 'FS001', 'Brown', 'Formal Business Shoes', 'Leather', 2000.00, '93866.jpg', 1, '2018-08-31 03:52:39', '2018-09-04 03:52:02'),
(35, 18, 'Puma Casual Shoes', 'PM100', 'White', '', '', 1000.00, '96384.jpg', 1, '2018-09-19 12:41:07', '2018-09-19 12:41:07'),
(36, 26, 'TV', 'TV101', 'Black', 'black tv', '', 1000.00, '39096.jpg', 0, '2018-10-26 05:06:27', '2018-10-26 05:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_attributes`
--

INSERT INTO `products_attributes` (`id`, `product_id`, `sku`, `size`, `price`, `stock`, `created_at`, `updated_at`) VALUES
(1, 27, 'BT01-S', 'Small', 1000.00, 10, '2018-08-23 05:34:10', '2018-08-23 05:34:10'),
(2, 27, 'BT01-M', 'Medium', 1200.00, 15, '2018-08-23 05:34:10', '2018-08-23 05:34:10'),
(3, 27, 'BT01-L', 'Large', 1500.00, 10, '2018-08-23 05:34:10', '2018-08-23 05:34:10'),
(4, 20, 'GTS001-S', 'Small', 1000.00, 3, '2018-08-28 23:10:20', '2018-09-25 23:16:07'),
(5, 20, 'GTS001-M', 'Medium', 1400.00, 2, '2018-08-28 23:10:20', '2018-09-25 23:16:07'),
(6, 20, 'GTS001-L', 'Large', 1600.00, 10, '2018-08-28 23:10:20', '2018-09-25 23:16:07'),
(7, 28, 'AD001-S', 'Small', 4000.00, 3, '2018-08-29 10:36:54', '2018-09-26 00:57:29'),
(8, 21, 'BTS001-S', 'Small', 1000.00, 10, '2018-09-17 10:25:27', '2018-09-17 10:25:27'),
(9, 31, 'CSS002-M', 'Medium', 1200.00, 11, '2018-09-17 10:42:08', '2018-09-20 03:43:45'),
(10, 31, 'BTS002-L', 'Large', 1700.00, 17, '2018-09-20 03:43:30', '2018-09-20 03:43:45'),
(11, 28, 'AD002-M', 'Medium', 4100.00, 11, '2018-09-26 00:57:12', '2018-09-26 00:57:29'),
(12, 28, 'AD003-L', 'Large', 4150.00, 10, '2018-09-26 00:57:13', '2018-09-26 00:57:29'),
(13, 36, 'TV-01', '40\"', 1200.00, 1, '2018-10-26 05:14:20', '2018-10-26 05:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_images`
--

INSERT INTO `products_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(4, 28, '28386.jpg', '2018-08-31 11:44:50', '2018-08-31 05:59:50'),
(5, 28, '21942.jpg', '2018-08-31 11:44:50', '2018-08-31 05:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `refund_policy`
--

CREATE TABLE `refund_policy` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `refund_policy`
--

INSERT INTO `refund_policy` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '<h2>Issuance of Refunds</h2>\r\n\r\n<p>If your product is eligible for a refund, you can choose your preferred refund method based on the table below. The shipping fee is refunded along with the amount paid for your returned product.</p>\r\n\r\n<p>The time required to complete a refund depends on the refund method you have selected. Once we have received your product (3-4 working days) and it has undergone a quality control (1-2 working days), the expected refund processing times are as follows:</p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Payment Method</th>\r\n			<th>Refund Option</th>\r\n			<th>Refund Time</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Cash on Delivery (COD)&nbsp; &nbsp; &nbsp; &nbsp;</td>\r\n			<td>Refund Voucher&nbsp; &nbsp; &nbsp;</td>\r\n			<td>1-2 working days</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Cash on Delivery (COD)</td>\r\n			<td>Bank Transfer</td>\r\n			<td>1-2 working days</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `title`, `icon`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Facebook', 'facebook', 'https://www.facebook.com/', 1, NULL, NULL),
(2, 'Twitter', 'twitter', 'https://twitter.com/', 1, NULL, NULL),
(3, 'Instagram', 'instagram', 'https://www.instagram.com/', 1, NULL, NULL),
(5, 'Youtube', 'youtube', 'https://www.youtube.com/', 1, NULL, NULL),
(6, 'LinkedIn', 'linkedin', 'https://www.linkedin.com/', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms_of_use`
--

CREATE TABLE `terms_of_use` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms_of_use`
--

INSERT INTO `terms_of_use` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, '<h1>Terms and Conditions</h1>\r\n\r\n<p>Please read the Terms and Conditions carefully before using Daraz.com.np.</p>\r\n\r\n<p>This electronic record is generated by a computer system and does not require any physical or digital signatures.</p>\r\n\r\n<h3>1.Introduction</h3>\r\n\r\n<p>Welcome to Daraz.com.np also hereby known as &ldquo;we&quot;, &quot;us&quot; or &quot;Daraz&quot;. We are an online marketplace and these are the terms and conditions governing your access and use of Daraz along with its related sub-domains, sites, mobile app, services and tools (the &quot;Site&quot;). By using the Site, you hereby accept these terms and conditions (including the linked information herein) and represent that you agree to comply with these terms and conditions (the &quot;User Agreement&quot;). This User Agreement is deemed effective upon your use of the Site which signifies your acceptance of these terms. If you do not agree to be bound by this User Agreement please do not access, register with or use this Site. This Site is owned and operated by Daraz Kaymu Pvt. Ltd.</p>\r\n\r\n<p>The Site reserves the right to change, modify, add, or remove portions of these Terms and Conditions at any time without any prior notification. Changes will be effective when posted on the Site with no other notice provided. Please check these Terms and Conditions regularly for updates. Your continued use of the Site following the posting of changes to Terms and Conditions of use constitutes your acceptance of those changes.&nbsp;</p>\r\n\r\n<h3>2. Conditions Of Use</h3>\r\n\r\n<h3>A. Your Account</h3>\r\n\r\n<p>To access certain services offered by the platform, we may require that you create an account with us or provide personal information to complete the creation of an account. We may at any time in our sole and absolute discretion, invalidate the username and/or password without giving any reason or prior notice and shall not be liable or responsible for any losses suffered by, caused by, arising out of, in connection with or by reason of such request or invalidation.</p>\r\n\r\n<p>You are responsible for maintaining the confidentiality of your user identification, password, account details and related private information. You agree to accept this responsibility and ensure your account and its related details are maintained securely at all times and all necessary steps are taken to prevent misuse of your account. You should inform us immediately if you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorized manner. You agree and acknowledge that any use of the Site and related services offered and/or any access to private information, data or communications using your account and password shall be deemed to be either performed by you or authorized by you as the case may be. You agree to be bound by any access of the Site and/or use of any services offered by the Site (whether such access or use are authorized by you or not). You agree that we shall be entitled (but not obliged) to act upon, rely on or hold you solely responsible and liable in respect thereof as if the same were carried out or transmitted by you. You further agree and acknowledge that you shall be bound by and agree to fully indemnify us against any and all losses arising from the use of or access to the Site through your account.</p>\r\n\r\n<p>Please ensure that the details you provide us with are correct and complete at all times. You are obligated to update details about your account in real time by accessing your account online. For pieces of information you are not able to update by accessing Your Account on the Site, you must inform us via our customer service communication channels to assist you with these changes. We reserve the right to refuse access to the Site, terminate accounts, remove or edit content at any time without prior notice to you. We may at any time in our sole and absolute discretion, request that you update your Personal Data or forthwith invalidate the account or related details without giving any reason or prior notice and shall not be liable or responsible for any losses suffered by or caused by you or arising out of or in connection with or by reason of such request or invalidation. You hereby agree to change your password from time to time and to keep your account secure and also shall be responsible for the confidentiality of your account and liable for any disclosure or use (whether such use is authorised or not) of the username and/or password.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>B. Privacy</h3>\r\n\r\n<p>Please review our Privacy Agreement, which also governs your visit to the Site. The personal information / data provided to us by you or your use of the Site will be treated as strictly confidential, in accordance with the Privacy Agreement and applicable laws and regulations. If you object to your information being transferred or used in the manner specified in the Privacy Agreement, please do not use the Site.</p>\r\n\r\n<h3>C. Platform For Communication</h3>\r\n\r\n<p>You agree, understand and acknowledge that the Site is an online platform that enables you to purchase products listed at the price indicated therein at any time from any location using a payment method of your choice. You further agree and acknowledge that we are only a facilitator and cannot be a party to or control in any manner any transactions on the Site or on a payment gateway as made available to you by an independent service provider. Accordingly, the contract of sale of products on the Site shall be a strictly bipartite contract between you and the sellers on our Site while the payment processing occurs between you, the service provider and in case of prepayments with electronic cards your issuer bank. Accordingly, the contract of payment on the Site shall be strictly a bipartite contract between you and the service provider as listed on our Site.</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `city`, `state`, `country`, `pincode`, `mobile`, `email`, `password`, `admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Hari', 'Satdobato', 'Kathmandu', 'Malangwa', 'Nepal', '44600', '9860000000', 'hari@gmail.com', '$2y$10$9Z1jERyXcDfmR0BdnP0rQ.BS.I6B9m52xhejjMX8hfGjr6GhQ13vm', NULL, 'f4E1DIc0EHrlCdXekF6VD0gByq5dnVXNRQTnQxNGcWCWNofl0cnwiykWXATf', '2018-08-16 03:56:04', '2018-09-26 00:27:07'),
(3, 'Avaya Baniya', 'Basantapur', 'Kathmandu', 'Bagmati', 'Nepal', '44600', '9860089363', 'baniyaavaya2@gmail.com', '$2y$10$zxdXo01IPYcjIm8T2kjCpuRzB0fnTXx4ML3Blm4.rfrH0sg3eLwru', NULL, 'pfYAz3tbS8LxIN9gRuLrdU0unjaqY7pEZiYiKcDQMk8YwaAnpexbyqTWSaV1', '2018-09-05 23:43:55', '2018-09-21 01:20:13'),
(4, 'Yeyur', NULL, NULL, NULL, NULL, NULL, NULL, 'yeyur@gmail.com', '$2y$10$FPc5shMw7CwaEfGjGAjL9eLBbqAmzos/hlYTTJpv6hv5.rJ.QYZfW', NULL, 'UXEOQlIF3UPz1dKdHCbH4eQ1CMltb2cj0yCiGyuACA5vujmc4OrMD4qDctW2', '2018-09-25 21:34:13', '2018-09-25 21:34:13'),
(5, 'mayur', NULL, NULL, NULL, NULL, NULL, NULL, 'mayur@gmail.com', '$2y$10$akHChu.ConlWhtdNES2neuxxzKnuUURM3Ug9lpE/RcXbqOk0bkqnS', NULL, 'qDkpBC7S9q1z9sgPbTebu6Uz5wtHBCpAbMa8wy1P8ZrDICEcU6U9VjB96l7i', '2018-09-25 21:44:08', '2018-09-25 21:44:08'),
(6, 'onlinezeal', 'Tinkune', 'Kathmandu', 'Bagmati', 'Nepal', '44600', '9860000000', 'onlinezeal@gmail.com', '$2y$10$lrujaDCp6AJUVoWXQjIRLuwdnLY.jzPfgJGMv6Jh04qP9LsKLeMGO', NULL, 'tK7HkZGmjDg6weGSBVAYwYtrGpYpD06uLTQmgCIOOhnMyz5vHw0zleG6pF1F', '2018-09-26 01:04:33', '2018-09-26 01:05:01'),
(7, 'asda', NULL, NULL, NULL, NULL, NULL, NULL, 'asd@gmail.com', '$2y$10$gSDJP6Neqvu7hzNKpvbCyOIua./MeKgIOJTD4vC9E/0cXtDqvAGLC', NULL, NULL, '2018-10-26 04:59:50', '2018-10-26 04:59:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_information`
--
ALTER TABLE `company_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_to_place_order`
--
ALTER TABLE `how_to_place_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_settings`
--
ALTER TABLE `page_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund_policy`
--
ALTER TABLE `refund_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_of_use`
--
ALTER TABLE `terms_of_use`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `company_information`
--
ALTER TABLE `company_information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_info`
--
ALTER TABLE `contact_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `how_to_place_order`
--
ALTER TABLE `how_to_place_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `page_settings`
--
ALTER TABLE `page_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `refund_policy`
--
ALTER TABLE `refund_policy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `terms_of_use`
--
ALTER TABLE `terms_of_use`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
