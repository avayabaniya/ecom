<?php
use \App\Http\Controllers\Controller;

$pageDetails = Controller::pageDetails();
?>


<!--Header-part-->
<div id="header">
  <h1><a href="{{ url('/admin/dashboard')}}">Matrix Admin</a></h1>
  <div style="margin-left: 25px;">
    <a href="{{ url('/admin/dashboard')}}">
      <img src="{{ asset ('public/images/backend_images/logo/'.$pageDetails->logo) }}" alt="Logo" />
    </a>
  </div>

</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    
    <li class=""><a title="" href="{{ url('/admin/settings')}}"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class=""><a title="" href="{{ url('/logout') }}"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<!--close-top-serch-->