<?php
use \App\Http\Controllers\Controller;
//$mainCategories = Controller::mainCategories();
$pendingOrder = Controller::pendingOrders();
$url = url()->current();

//echo $url;die;
?>
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li <?php if (preg_match("/dashboard/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/dashboard') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Page Settings</span> <span class="label label-important">6</span></a>
      <ul <?php if (preg_match("/page-settings/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/view-page-logo/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-page-logo')  }}">View Page Details</a></li>
        <li <?php if (preg_match("/view-how-to-place-order/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-how-to-place-order')  }}">Place Order</a></li>
        <li <?php if (preg_match("/view-faq/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-faq')  }}">FAQ</a></li>
        <li <?php if (preg_match("/view-terms-of-use/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-terms-of-use')  }}">Terms of Use</a></li>
        <li <?php if (preg_match("/view-refund-policy/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-refund-policy')  }}">Refund Policy</a></li>
        <li <?php if (preg_match("/view-company-information/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/page-settings/view-company-information')  }}">Company Information</a></li>
      </ul>
    </li>

    {{--Careers--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Career</span> <span class="label label-important">2</span></a>
      <ul <?php if (preg_match("/career/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/add-career/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-career')  }}">Add Career</a></li>
        <li <?php if (preg_match("/view-career/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-careers')  }}">View Careers</a></li>
      </ul>
    </li>

    {{--Payment Type--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Payment Type</span> <span class="label label-important">2</span></a>
      <ul <?php if (preg_match("/payment/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/add-payment-type/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-payment-type')  }}">Add Payment type</a></li>
        <li <?php if (preg_match("/view-payment-types/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-payment-types')  }}">View Payment Types</a></li>
      </ul>
    </li>

    {{--Category--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Categories</span> <span class="label label-important">2</span></a>
      <ul <?php if (preg_match("/categor/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/add-category/i", $url)) { ?> class="active" <?php } ?> ><a href="{{ url('/admin/add-category')  }}">Add Category</a></li>
        <li <?php if (preg_match("/view-categories/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-categories')  }}">View Categories</a></li>
      </ul>
    </li>
    {{--Product--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Products</span> <span class="label label-important">2</span></a>
    <ul <?php if (preg_match("/product/i", $url)) { ?> style="display: block; " <?php } ?>>
      <li <?php if (preg_match("/add-product/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-product')  }}">Add Product</a></li>
      <li <?php if (preg_match("/view-product/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-products')  }}">View products</a></li>
    </ul>
    </li>
    {{--Coupons--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Coupons</span> <span class="label label-important">2</span></a>
      <ul <?php if (preg_match("/coupon/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/add-coupon/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-coupon')  }}">Add Coupon</a></li>
        <li <?php if (preg_match("/view-coupon/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-coupons')  }}">View Coupons</a></li>
      </ul>
    </li>
    {{--Banner--}}
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Banners</span> <span class="label label-important">2</span></a>
      <ul <?php if (preg_match("/banner/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/add-banner/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-banner')  }}">Add Banner</a></li>
        <li <?php if (preg_match("/view-banner/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-banners')  }}">View Banners</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Contact Us</span> <span class="label label-important">3</span></a>
      <ul <?php if (preg_match("/contact/i", $url)) { ?> style="display: block; " <?php } ?>>
        <li <?php if (preg_match("/view-contact-info/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-contact-info')  }}">View Contact Info</a></li>
        <li <?php if (preg_match("/view-contact-social-media/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-contact-social-media')  }}">View Social Medias</a></li>
        <li <?php if (preg_match("/add-contact-social-media/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/add-contact-social-media')  }}">Add Social Media</a></li>
      </ul>
    </li>
      <li <?php if (preg_match("/view-orders/i", $url)) { ?> class="active" <?php } ?>><a href="{{ url('/admin/view-orders') }}"><i class="icon icon-list"></i> <span>View Orders</span><span class="label label-important"><i class="icon icon-bell" style="padding-bottom: 2px;"></i>{{ $pendingOrder}}</span></a></li>
  </ul>
</div>
<!--sidebar-menu-->