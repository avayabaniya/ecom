<?php
    use \App\Http\Controllers\Controller;
    $socialMedias = Controller::socialMedia();
    $mainCategories = Controller::mainCategories();
    $pageDetails = Controller::pageDetails();
?>


<footer id="footer"><!--Footer-->

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="companyinfo" style="margin-top: 0px;">
                        {{--<h2><span>e</span>-shopper</h2>--}}
                        <h2><span>About Us</span></h2>
                        <p style="font-weight: bold;">
                            {!! $pageDetails->description !!}
                        </p>
                    </div>
                </div>

                <div class="col-sm-3 col-sm-offset-1" style="margin-top: 30px;">
                    <div class="logo pull-right">
                        <a href="{{ url('/') }}"><img src="{{ asset('public/images/backend_images/logo/'.$pageDetails->logo) }}" width="200px" /></a>
                    </div>
                </div>

            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Support & Service</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('/contact-us') }}">Need Help</a></li>
                            <li><a href="{{ url('/support-and-services/how-to-place-order') }}">How to place Order</a></li>
                            <li><a href="{{ url('/support-and-services/faq') }}">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Terms & Condition</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('/terms-and-condition/terms-of-use') }}">Terms of Use</a></li>
                            <li><a href="{{ url('/terms-and-condition/refund-policy') }}">Refund Policy</a></li>
                            <li><a href="{{ url('/terms-and-condition/payment-methods') }}">Payment Methods</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>About Company</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('/about-company/company-information') }}">Company Information</a></li>
                            <li><a href="{{ url('/about-company/careers') }}">Careers</a></li>
                            <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Quick Shop</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <?php $i = 0 ?>
                            @foreach($mainCategories as $cat)
                                @if($cat->status == "1")
                                        <?php $i++ ?>
                                    <li><a href="{{ asset('products/'.$cat->url) }}">{{ $cat->name }}</a></li>
                                            @if($i > 2)
                                                @break
                                            @endif
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="single-widget">

                        <div class="social-networks" style="margin-top: 50px;">
                            <h2 class="title text-center">Stay Connected</h2>
                            <ul>
                                @foreach($socialMedias as $socialMedia)
                                    @if($socialMedia->status == 1)
                                        <li>
                                            <a href="{{ $socialMedia->link }}"><i class="fa fa-{{ $socialMedia->icon }}"></i></a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © {{ date('Y') }} E-SHOPPER Inc. All rights reserved.</p>
                <p class="pull-right">Designed by <span><a target="_blank" href="http://www.onlinezeal.com">OnlineZeal</a></span></p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->