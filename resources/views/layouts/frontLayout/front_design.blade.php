<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link rel="icon" type="image/png" href="{{ asset('public/images/frontend_images/favicon.png') }}">
    <link href=" {{ asset('public/css/frontend_css/bootstrap.min.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/font-awesome.min.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/prettyPhoto.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/price-range.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/animate.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/main.css') }} " rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/responsive.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/easyzoom.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/passtrength.css') }}" rel="stylesheet">
    <link href=" {{ asset('public/css/frontend_css/font-awesome.min.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src=" {{ asset('public/js/frontend_css/html5shiv.js') }}"></script>
    <script src=" {{ asset('public/js/frontend_css/respond.min.js') }}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('public/images/frontend_images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('public/images/frontend_images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('public/images/frontend_images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('public/images/frontend_images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('public/images/frontend_images/ico/apple-touch-icon-57-precomposed.png') }}">
</head><!--/head-->

<body>
@include('layouts.frontLayout.front_header')

@yield('content')

@include('layouts.frontLayout.front_footer')


{{--<script src="https://use.fontawesome.com/c4c67c5c26.js"></script>--}}
<script src=" {{ asset('public/js/frontend_js/fontawesome.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/jquery.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/bootstrap.min.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/jquery.scrollUp.min.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/price-range.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/jquery.prettyPhoto.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/easyzoom.js') }}"></script>
<script src=" {{ asset('public/js/frontend_js/main.js') }}"></script>
<script src="{{ asset('public/js/frontend_js/jquery.validate.js') }}"></script>
<script src="{{ asset('public/js/frontend_js/passtrength.js') }}"></script>
{{--<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script src=" {{ asset('js/backend_js/jquery.min.js') }}"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
</body>
</html>