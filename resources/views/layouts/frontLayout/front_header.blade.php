<?php
    use \App\Http\Controllers\Controller;
    $mainCategories = Controller::mainCategories();
    $url = url()->current();
    $cartCount = Controller::cartCount();
    $orderCount = Controller::orderCount();
    $pageDetails = Controller::pageDetails();
?>

<header id="header"><!--header-->
    <!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{ url('/') }}"><img src="{{ asset('public/images/backend_images/logo/'.$pageDetails->logo) }}" alt="" /></a>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('/checkout') }}" <?php if (preg_match("/checkout/i", $url)) { ?> class="active" <?php } ?>><i class="fa fa-crosshairs"></i> Checkout</a></li>
                            <li><a href="{{ url('/cart') }}" <?php if (preg_match("/cart/i", $url)) { ?> class="active" <?php } ?>><i class="fa fa-shopping-cart"></i>Cart <i class="badge badge-light active">{{ $cartCount }}</i></a></li>
                            @if(empty(Session::has('Email')))
                                <li><a href="{{ url('/login-register') }}" <?php if (preg_match("/login-register/i", $url)) { ?> class="active" <?php } ?>><i class="fa fa-lock"></i> Login</a></li>
                            @else
                                <li><a href="{{ url('/track-orders') }}" title="View Placed Orders" <?php if (preg_match("/track-orders/i", $url)) { ?> class="active" <?php } ?>><i class="fa fa-gift"></i>Orders <i class="badge badge-light active">{{ $orderCount }}</i></a></li>
                                <li><a href="{{ url('/account') }}" title="View Profile" <?php if (preg_match("/account/i", $url)) { ?> class="active" <?php } ?>><i class="fa fa-user"></i>{{ Auth::User()->name }}</a></li>
                                <li><a href="{{ url('/user-logout') }}" ><i class="fa fa-sign-out"></i> Logout</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ url('/') }}"  <?php if (substr($url,-1) == "e") { ?> class="active" <?php } ?> >Home</a></li>
                            <li class="dropdown"><a href="#" <?php if (preg_match("/products/i", $url)) { ?> class="active" <?php } ?>>Shop<i class="fa fa-angle-down" ></i></a>
                                <ul role="menu" class="sub-menu">
                                    @foreach($mainCategories as $cat)
                                        @if($cat->status == "1")
                                            <li><a href="{{ asset('products/'.$cat->url) }}">{{ $cat->name }}</a></li>
                                        @endif
                                    @endforeach

                                </ul>
                            </li>

                            <li><a href="{{ url('/contact-us') }}" <?php if (preg_match("/contact-us/i", $url)) { ?> class="active" <?php } ?> >Contact</a></li>
                        </ul>
                    </div>
                </div>
                {{--<div class="col-sm-3">
                    <div class="search_box pull-right">
                        <input type="text" placeholder="Search"/>
                    </div>
                </div>--}}
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->