@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">View Payment Types</a> </div>
            <h1>Products</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Payment Types</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Payment Title</th>
                                    <th>Payment Type</th>
                                    <th>Description</th>
                                    <th>URL</th>
                                    <th>Status</th>
                                    <th>Image</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($paymentTypes as $paymentType)
                                    <tr class="gradeX">
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $paymentType->title }}</td>
                                        <td>{{ $paymentType->payment_type }}</td>
                                        <td>{{ $paymentType->description }}</td>
                                        <td>{{ $paymentType->url }}</td>
                                        <td>
                                            @if( $paymentType->enable == 0 )
                                                Inactive
                                            @else
                                                Active
                                            @endif</td>
                                        <td>
                                            @if(!empty($paymentType->image))
                                                <img src="{{ asset('public/images/backend_images/payment_types/small/'.$paymentType->image) }}" style="width: 75px;" >
                                            @endif
                                        </td>
                                        <td class="center">
                                            <a href="{{ url('admin/edit-payment-type/'.$paymentType->id) }}" class="btn btn-primary btn-mini" title="Edit Payment Type">Edit</a>
                                            {{--delProduct class for pop up the other for design--}}
                                            <a rel="{{ $paymentType->id }}" rel1="delete-payment-type" href="javascript:" class="btn btn-danger btn-mini deleteRecord" title="Delete Product">Delete</a>
                                            {{--onclick="return confirm('Are you fucking sure ? ')"--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection