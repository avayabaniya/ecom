@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>  <a href="#" class="current">Edit Payment Type</a> </div>
            <h1>Payment Types</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Edit Payment Type</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form class="form-horizontal" method="post" action="{{ url('/admin/edit-payment-type/'.$paymentTypeDetails->id) }}" name="add_paymentType" id="add_paymentType" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="control-group">
                                    <label class="control-label">Payment Title</label>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" value="{{ $paymentTypeDetails->title }}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Payment Type</label>
                                    <div class="controls">
                                        <select name="payment_type" style="width: 220px;">
                                            <option value="online" @if($paymentTypeDetails->payment_type == "online") selected @endif>Online Payment</option>
                                            <option value="offline" @if($paymentTypeDetails->payment_type == "offline") selected @endif>Offline Payment</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Description</label>
                                    <div class="controls">
                                        <textarea name="description" id="summary_ckeditor">{{ $paymentTypeDetails->description }}</textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">URL</label>
                                    <div class="controls">
                                        <input type="text" name="url" id="url" value="{{ $paymentTypeDetails->url }}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Images</label>
                                    <div class="controls">
                                        {{--Upload new image--}}
                                        <input type="file" name="image" id="image">
                                        {{--Current image file name--}}
                                        <input type="hidden" name="current_image" value="{{ $paymentTypeDetails->image }}">
                                        {{--Display old image--}}
                                        @if(!empty($paymentTypeDetails->image))
                                            <img style="width: 40px" src="{{ asset('public/images/backend_images/payment_types/small/'.$paymentTypeDetails->image) }}"> |
                                            <a href="{{ url('admin/delete-payment-type-image/'.$paymentTypeDetails->id) }}">Delete</a>
                                        @endif
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Enable</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" @if($paymentTypeDetails->enable == 1) checked @endif value="1">
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="Edit payment Type" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection