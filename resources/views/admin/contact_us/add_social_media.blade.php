@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">Add Social Media</a> </div>
            <h1>Social Media</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Add Social Media</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form class="form-horizontal" method="post" action="{{ url('/admin/add-contact-social-media') }}" name="add_social_media" id="add_social_media" novalidate="novalidate">
                                {{ csrf_field() }}
                                <div class="control-group">
                                    <label class="control-label" for="social_media_title">Title</label>
                                    <div class="controls">
                                        <input type="text" name="social_media_title" id="social_media_title">
                                    </div>
                                </div>
                                {{--Category Level--}}
                                <div class="control-group">
                                    <label class="control-label">Select Social Media</label>
                                    <div class="controls">
                                        <select name="social_media_icon" style="width: 220px;">
                                            <option selected disabled>--Select Social Media--</option>
                                            <option value="facebook">facebook</option>
                                            <option value="twitter">twitter</option>
                                            <option value="instagram">instagram</option>
                                            <option value="google-plus">google plus</option>
                                            <option value="youtube">youtube</option>
                                            <option value="linkedin">linkedin</option>
                                            <option value="dribbble">dribbble</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="social_media_link">Link</label>
                                    <div class="controls">
                                        <input type="text" name="social_media_link" id="social_media_link">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="status">Enable</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" value="1" style="opacity: 1;">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="Add social media" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection