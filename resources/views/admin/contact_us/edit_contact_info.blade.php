@extends('layouts.adminLayout.admin_design')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">Edit Contact Information</a> </div>
            <h1>Contact Info</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Edit Contact Information</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <input type="hidden" name="contact_info_id" value="{{ $contactInfo->id }}">
                            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/edit-contact-info/'.$contactInfo->id) }}" name="edit_contact_info" id="edit_contact_info">
                                {{ csrf_field() }}

                                <div class="control-group">
                                    <label class="control-label">Title:</label>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" value="{{ $contactInfo->title }}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Country: </label>
                                    <div class="controls">
                                        <select id="edit_country" name="edit_country" style="width: 220px;">
                                            <option value="">Select Country</option>
                                            @foreach($countries as $country)
                                                <option value="{{ $country->country_name }}" @if($country->country_name == $contactInfo->country) selected @endif>{{ $country->country_name }}</option>
                                            @endforeach
                                        </select>
                                        {{--<input type="text" name="country" id="country" value="{{ $contactInfo->country }}">--}}
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">District: </label>
                                    <div class="controls">
                                        <input type="text" name="district" id="district" value="{{ $contactInfo->district }}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Address: </label>
                                    <div class="controls">
                                        <input type="text" name="address" id="address" value="{{ $contactInfo->address }}">
                                    </div>
                                    </div>

                                <div class="control-group">
                                    <label class="control-label">Number: </label>
                                    <div class="controls">
                                        <input type="text" name="number" id="number" value="{{ $contactInfo->number }}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Email: </label>
                                    <div class="controls">
                                        <input type="text" name="email" id="email" value="{{ $contactInfo->email }}">
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="Update Contact Information" class="btn btn-success">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection