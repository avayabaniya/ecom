@extends('layouts.adminLayout.admin_design')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">Contact Information</a> </div>
            <h1>Contact Info</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>View Contact Information</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <input type="hidden" name="contact_info_id" value="{{ $contactInfo->id }}">
                            <div class="form-horizontal">
                                {{ csrf_field() }}

                                <div class="control-group">
                                    <label class="control-label">Title:</label>
                                    <label class="control-label"><strong>{{ $contactInfo->title }}</strong></label>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Country: </label>
                                    <label class="control-label"><strong>{{ $contactInfo->country }}</strong></label>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">District: </label>
                                    <label class="control-label"><strong>{{ $contactInfo->district }}</strong></label>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Address: </label>
                                    <label class="control-label"><strong>{{ $contactInfo->address }}</strong></label>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Number: </label>
                                    <label class="control-label"><strong>{{ $contactInfo->number }}</strong></label>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Email: </label>
                                    <label class="control-label"><strong>{{ $contactInfo->email }}</strong></label>
                                </div>


                                    <a href="{{ url('admin/edit-contact-info/'.$contactInfo->id) }}" class="btn btn-primary" style="margin: 20px;">Edit Contact Info</a>

                            </div></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection