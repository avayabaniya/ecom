@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">View Social Media</a> </div>
            <h1>Social Media</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Social Media</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>Social Media ID</th>
                                    <th>Title</th>
                                    <th>Icon</th>
                                    <th>Link</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($socialMedias as $socialMedia)
                                    <tr class="gradeX">
                                        <td>{{ $socialMedia->id }}</td>
                                        <td>{{ $socialMedia->title }}</td>
                                        <td>
                                            <i class="fa fa-{{ $socialMedia->icon }}" style="font-size:36px; margin-left: 25px;"></i>
                                        </td>
                                        <td>{{ $socialMedia->link }}</td>
                                        <td>
                                            @if($socialMedia->status == 1) Active @else Inactive @endif
                                        </td>
                                        <td class="center">
                                            <a href="{{ url('admin/edit-contact-social-media/'.$socialMedia->id) }}" class="btn btn-primary btn-mini">Edit</a>
                                            {{--<a href="{{ url('admin/delete-category/'.$category->id) }}" class="btn btn-danger btn-mini" onclick="return confirm('Are you fucking sure ? ')">Delete</a>--}}
                                            <a rel="{{ $socialMedia->id }}" rel1="delete-contact-social-media" href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection