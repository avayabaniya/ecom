@extends('layouts.adminLayout.admin_design')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a>   <a href="#" class="current">View Orders</a> </div>
            <h1>Orders</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View Orders</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Product</th>
                                    <th>Order Type</th>
                                    <th>Payment Method</th>
                                    <th>Size</th>
                                    <th>Quantity</th>
                                    <th>Order Price</th>
                                    <th>User Email</th>
                                    <th>Order Status</th>
                                    <th>Set Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr class="gradeX" @if($order->order_status == 'completed') style="background-color: lightgray; @endif">
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>
                                            <a href="#myModal{{ $order->product_id }}" data-toggle="modal" title="View Product">
                                                @if(!empty($order->product->image))
                                                    <img src="{{ asset('public/images/backend_images/products/small/'.$order->product->image) }}" style="width: 75px;" >
                                                @endif
                                            </a>
                                        </td>
                                        <td>{{ $order->order_type }}</td>
                                        <td>{{ $order->payment_type }}</td>
                                        <td>
                                            {{$order->size}}
                                        </td>
                                        <td>
                                            {{$order->quantity}}
                                        </td>
                                        <td>{{ $order->order_price }}</td>
                                        <td>
                                            {{ $order->user_email }} |
                                            {{--<a href="#exampleModal{{ $order->user->id }}" data-toggle="modal" class="glyphicon glyphicon-envelope-eye-open" title="View User">View User</a>--}}
                                            <a href="#myAlert{{ $order->user->id }}" data-toggle="modal" class="btn btn-success btn-mini" title="View User">View User</a>
                                        </td>
                                        <td style="font-weight: bold";>{{ $order->order_status }}</td>
                                        <td>
                                            <form id="store" name="store" method="post" action="{{ url('/admin/edit-order') }}">
                                            {{csrf_field()}}
                                                <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                <select name="status" id="status" onchange="this.form.submit()">
                                                    <option value="pending" @if($order->order_status == 'pending') selected @endif>Pending</option>
                                                    <option value="on-way" @if($order->order_status == 'on-way') selected @endif>On way</option>
                                                    <option value="completed" @if($order->order_status == 'completed') selected @endif>Completed</option>
                                                </select>
                                            </form>
                                        </td>
                                    </tr>

                                    <div id="myModal{{ $order->product_id }}" class="modal hide">
                                        <div class="modal-header">
                                            <button data-dismiss="modal" class="close" type="button">×</button>
                                            <h3>{{ $order->product->product_name }} Full Details</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p><h5 style="display: inline-block;">Product ID:</h5> {{ $order->product->id }}</p>
                                            <p>
                                                @if(!empty($order->product->image))
                                                    <img src="{{ asset('public/images/backend_images/products/medium/'.$order->product->image) }}" style="width: 150px;" >
                                                @endif
                                            </p>
                                            <p><h5 style="display: inline-block;">Product Name:</h5> {{ $order->product->product_name }}</p>
                                            <p><h5 style="display: inline-block;">Product Code:</h5> {{ $order->product->product_code }}</p>
                                            <p><h5 style="display: inline-block;">Product Size:</h5> {{ $order->size}}</p>
                                            <p><h5 style="display: inline-block;">Product Description:</h5> {!! $order->product->description !!}</p>
                                            <p><h5 style="display: inline-block;">Base Product Price:</h5> {{ $order->product->price }}</p>
                                            <p><h5 style="display: inline-block;">Quantity:</h5> {{ $order->quantity }}</p>
                                            <p><h5 style="display: inline-block;">Final Checkout Price:</h5> {{ $order->order_price }}</p>
                                        </div>
                                    </div>


                                    <div class="modal hide" id="myAlert{{ $order->user->id }}" aria-hidden="true" style="display: none;">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal">×</button>
                                            <h3>{{$order->user->name}}</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p><h5 style="display: inline-block;">User ID:</h5> {{ $order->user->id }}</p>
                                            <p><h5 style="display: inline-block;">User Name:</h5> {{ $order->user->name }}</p>
                                            <p><h5 style="display: inline-block;">City:</h5> {{ $order->user->city }}</p>
                                            <p><h5 style="display: inline-block;">Contact Info:</h5> {{ $order->user->mobile }}</p>
                                            <p><h5 style="display: inline-block;">Email:</h5> {{ $order->user->email }}</p>
                                        </div>
                                    </div>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection