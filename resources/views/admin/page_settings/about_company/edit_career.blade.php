@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{url('/admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>Home</a> s  <a href="#" class="current">View Page Settings</a> </div>
            <h1>Careers</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif

            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Edit Career</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/edit-career/'.$career->id)}}" name="edit_career" id="edit_career">
                                {{ csrf_field() }}

                                <div class="control-group">
                                    <label class="control-label">Career Title</label>
                                    <div class="controls">
                                        <input value="{{ $career->title }}" type="text" name="title" id="title" maxlength="30" required>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Department</label>
                                    <div class="controls">
                                        <input value="{{ $career->department }}" type="text" name="department" id="department" maxlength="30" required>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Job Type</label>
                                    <div class="controls">
                                        <select name="job_type" name="job_type" style="width: 220px;">
                                            <option @if($career->type == "fulltime") selected @endif value="fulltime">Full Time</option>
                                            <option @if($career->type == "parttime") selected @endif value="parttime">Part Time</option>
                                            <option @if($career->type == "intern") selected @endif value="intern">Internship</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="Update" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
