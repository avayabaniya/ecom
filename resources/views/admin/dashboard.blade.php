@extends('layouts.adminLayout.admin_design')
@section('content')

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="{{ url('/admin/dashboard') }}"> <i class="icon-dashboard"></i> <span class="label label-important"></span> My Dashboard </a> </li>
        <li class="bg_lg span3"> <a href="{{url('/admin/page-settings/view-page-logo')}}"> <i class="icon-th-list"></i> Page Settings</a> </li>
        <li class="bg_ly"> <a href="{{url('/admin/view-careers')}}"> <i class="icon-inbox"></i><span class="label label-success"></span> Careers </a> </li>
        <li class="bg_lo"> <a href="{{url('/admin/view-products')}}"> <i class="icon-th"></i> Products</a> </li>
        <li class="bg_ls"> <a href="{{url('/admin/view-orders')}}"> <i class="icon-fullscreen"></i> View Orders</a> </li>
      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>Site Analytics</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
              <div class="chart"></div>
            </div>
            <div class="span3">
              <ul class="site-stats">
                <li class="bg_lh"><i class="icon-user"></i> <strong>{{ $totalUsers }}</strong> <small>Total Users</small></li>
                <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong>{{$totalProducts}}</strong> <small>Total Products</small></li>
                <li class="bg_lh"><i class="icon-tag"></i> <strong>{{$totalOrders}}</strong> <small>Total Orders</small></li>
                <li class="bg_lh"><i class="icon-repeat"></i> <strong>{{$pendingOrders}}</strong> <small>Pending Orders</small></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box--> 
    <hr/>
  </div>
</div>

<!--end-main-container-part-->


@endsection