@component('mail::message')
# Introduction

Hello {{$user->user_name}}, your order has been successfully confirmed.

<strong>Product Name:</strong> {{ $order->product->product_name }}

<strong>Order Price:</strong> {{ $order->order_price }}

@component('mail::button', ['url' => 'http://localhost/ecom/'])
Keep Shopping
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
