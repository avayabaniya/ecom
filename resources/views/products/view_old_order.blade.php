@extends('layouts.frontLayout.front_design')
@section('content')

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="{{ asset('/') }}">Home</a></li>
                    <li class="active">Track Order</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                @if(Session::has('flash_message_success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('flash_message_success') !!}</strong>
                    </div>
                @endif

                @if(Session::has('flash_message_error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{!! session('flash_message_error') !!}</strong>
                    </div>
                @endif

                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Base Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Order Total</td>
                        <td class="price">Status</td>

                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td class="cart_product" style="margin-right: 0.1px;">
                                <a href=""><img style="width: 100px;" src="{{ asset('public/images/backend_images/products/medium/'.$order->product->image) }}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{ $order->product->product_name }}</a></h4>
                                <p>Code: {{ $order->product->product_code }}</p>
                                <p>Size: {{ $order->size }}</p>
                            </td>

                            <td class="cart_price">
                                <p>Rs. {{ $order->product->price }}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{ $order->quantity }}" autocomplete="off" size="2" readonly>

                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                    Rs.{{ $order -> order_price }}
                                </p>
                            </td>
                            <td class="cart_price">
                                <p class="cart_total_price">{{ucfirst($order->order_status)}}</p>
                            </td>
                            <td class="cart_delete">
                                @if($order->order_status === "pending")
                                    <a class="cart_quantity_delete" href="{{ url('/track-orders/delete-order/'.$order->id) }}" title="Cancel Order"><i class="fa fa-times"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->


@endsection

