@extends('layouts.frontLayout.front_design')
@section('content')

    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-2">

                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Payment Method</h2>
                        @foreach($paymentTypes as $paymentType)
                            @if($paymentType->enable == 1)
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{ asset('public/images/backend_images/payment_types/small/'.$paymentType->image ) }} " style="width: 170px" />
                                            <h2>{{ $paymentType->title }}</h2>
                                            <p>{!! $paymentType->description !!}} </p>
                                            <a href="{{ url('/confirm-order/'.$paymentType->url) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Select Method</a>
                                        </div>
                                        {{--<div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>Rs. {{ $product->price }}</h2>
                                                <p>{{ $product->product_name }}</p>
                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                            </div>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div><!--features_items-->

                    <!--/recommended_items-->
                    <div class="recommended_items"><!--recommended_items-->

                        <div class="category-tab"><!--category-tab-->
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li style="text-align: center"><a>Recommended Items</a></li>
                                </ul>
                            </div>
                        </div>



                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $count=1; ?>
                                @foreach($relatedProducts->chunk(3) as $chunk)
                                    <div <?php if($count==1) {?> class="item active" <?php } else { ?> class="item" <?php } ?>>
                                        @foreach($chunk as $item)
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products">
                                                        <div class="productinfo text-center">
                                                            <img style="width: 230px;" src="{{ asset(('public/images/backend_images/products/small/'.$item->image)) }}" alt="" />
                                                            <h2>Rs. {{ $item->price }}</h2>
                                                            <p>{{ $item->name }}</p>
                                                            <a href="{{ url('product/'.$item->id) }}"><button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <?php $count++; ?>
                                @endforeach
                            </div>
                            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div><!--/recommended_items-->


                </div>
            </div>
        </div>
    </section>

@endsection