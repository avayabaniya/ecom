@extends('layouts.frontLayout.front_design')
@section('content')


    <div class="container">
        <div class="bg">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title text-center">Careers</h2>
                    <div id="gmap" class="contact-map">
                        <table class="table table-condensed">
                            <thead>
                            <tr class="cart_menu">
                                <th>S.no</th>
                                <th>Title</th>
                                <th class="price">Department</th>
                                <th class="quantity">Job Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            @foreach($careers as $career)
                                <tr>
                                    <td>
                                        <?php echo $i; $i++; ?>
                                    </td>

                                    <td class="cart_description">
                                        <p>{{ $career->title }}</p>
                                    </td>

                                    <td class="cart_description">
                                        <p>{{ $career->department }}</p>
                                    </td>

                                    <td class="cart_description">
                                        <p>{{ $career->type }}</p>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection