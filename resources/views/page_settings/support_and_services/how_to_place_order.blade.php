@extends('layouts.frontLayout.front_design')
@section('content')


    <div class="container">
        <div class="bg">
            <div class="row">
                <div class="col-sm-12" style="margin-bottom: 50px;">
                    <h2 class="title text-center">How to Place <strong>Order?</strong></h2>

                        {!! $info->description  !!}

                </div>
            </div>
        </div>
    </div>

@endsection