$ = jQuery.noConflict();
/*price range*/

 /*$('#sl2').slider();*/

	var RGBChange = function() {
	  $('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
	};	
		
/*scroll to top*/
$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});

/*$(document).ready(function(){
    $("#selSize").change(function () {
        var idSize = $(this).val();
        alert(idSize);
    });
});*/

$(document).ready(function(){

	//Change Price and Stock with size
	$("#selSize").change(function () {
		var idSize = $(this).val();
		if(idSize == "") {
			return false;
		}
		$.ajax({
			type:'get',
			url:'/ecom/public/get-product-price',
			data:{idSize:idSize},
			success:function(resp) {
				/*alert(resp); return false;
                console.log(data);*/
				var arr = resp.split('#');
				$("#getPrice").html("Rs. "+ arr[0]);
				$("#price").val(arr[0]);
				//Send the updated price based on size of the product
				if (arr[1] == 0){
					$("#cartButton").hide();
					$("#Availability").text(" Out of Stock");
				}else {
                    $("#cartButton").show();
                    $("#Availability").text(" In Stock");
				}
            },error:function (resp) {
				alert("Error " + resp);
            }
		});
    });

	//Replace Main Image with Alternate Image
	/*$(".changeImage").click(function () {
		alert("test");
    })*/
});

//Replace Main Image with Alternate Image
$(document).ready(function () {
    $(".changeImage").click(function () {
        var image = $(this).attr('src');
        $(".mainImage").attr("src", image);
    })


	// Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function(e) {
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });

    // Setup toggles example
    var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

    $('.toggle').on('click', function() {
        var $this = $(this);

        if ($this.data("active") === true) {
            $this.text("Switch on").data("active", false);
            api2.teardown();
        } else {
            $this.text("Switch off").data("active", true);
            api2._init();
        }
    });

});


$().ready(function () {
    var reg = /^\d+$/;
	//Validate Register form on keyup and submit
	$('#registerForm').validate({
		rules:{
			name:{
				required:true,
				minlength:2,
				accept:"[a-zA-Z]+"
			},
			password:{
				required:true,
				minlength:6
			},
			email:{
				required:true,
				email:true,
				remote:"/ecom/check-email"
			},
            number:{
			    required: true,
                accept: "[1-9]\\d*|0\\d+",
                minlength:10
            },
            address:{
			    required:true
            },
            city:{
			    required:true
            }
		},
		messages:{
			name:{
				required:"Please enter your Name",
				minlength:"Your Name must be atleast 2 characters long",
				accept:"Your Name can only contain letters"

			},
			password:{
				required:"Please provide your Password",
				minlength: "Your Password must be atleast 6 chracters long"
			},
			email:{
				required: "Please enter your Email",
				email: "Please enter valid Email",
				remote: "Email already exists!"
			},
            number:{
			    required: "Please enter your contact number",
                accept: "Please enter a valid phone number",
                minlength: "Please enter a valid phone number"
            },
            address:{
			    required: "Please enter your address"
            },
            city:{
                required: "Please enter your city"
            }
		}
	});

    $('#accountForm').validate({
        rules:{
            name:{
                required:true,
                minlength:2,
                accept:"[a-zA-Z]+"
            },
            address:{
                required:true
            },
            city:{
                required:true
            },
			state:{
                required:true
            },
            country:{
                required:true
            },
            email:{
                required:true,
                email:true,
                remote:"/ecom/check-email"
            }
        },
        messages:{
            name:{
                required:"Please enter your Name",
                minlength:"Your Name must be atleast 2 characters long",
                accept:"Your Name can only contain letters"

            },
            address:{
                required:"Please provide your Address"
            },
            city:{
                required:"Please provide your City"
            },
            state:{
                required:"Please provide your State"
            },
			country:{
                required:"Please select your Country"
            }
        }
    });

	//Validate login
    $('#loginForm').validate({
        rules:{
            email:{
                required:true,
                email:true
            },
        	password:{
                required:true
            }
        },
        messages:{
            email:{
                required: "Please enter your Email",
                email: "Please enter valid Email"
            },
            password:{
                required:"Please provide your Password"
            }
        }
    });

    //Check Current user Password
    $('#current_pwd').keyup(function () {
        var current_pwd = $(this).val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
           type:'post',
           url: '/ecom/check-user-pwd',
           data:{current_pwd:current_pwd},
           success:function (resp) {
               /*alert(resp);*/
               if (resp == 'false'){
                   $('#chkPwd').html("<font color='red'>Current Password is incorrect</font>");
               }else if (resp == 'true'){
                   $('#chkPwd').html("<font color = 'green'>Current Password id correct</font>");
               }
           }, error:function () {
                alert("Error");
            }
        });
    });

	//Password Strength Script
	$('#myPassword').passtrength({
		minChars:6,
		passwordToggle: true,
		tooltip: true,
		eyeImg: "/ecom/public/images/frontend_images/eye.svg"
	});

});