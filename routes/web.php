<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

Route::get('/', 'IndexController@index');

//Route::match(['get','post'], '/admin', 'AdminController@index');


Route::match(['get','post'], '/admin', 'AdminController@login')->name('admin.login');




//Route::get('/admin/dashboard','AdminController@dashboard');



Route::group(['middleware' => ['auth:admin']],function(){
    Route::get('/logout','AdminController@logout');

	Route::get('/admin/dashboard','AdminController@dashboard');	
	Route::get('/admin/settings','AdminController@settings');
	Route::get('/admin/check-pwd','AdminController@chkPassword');
	Route::match(['get','post'],'/admin/update-pwd','AdminController@updatePassword');

	//Categories Route (Admin)
    Route::match(['get','post'], '/admin/add-category', 'CategoryController@addCategory');
    Route::match(['get', 'post'],'/admin/edit-category/{id}','CategoryController@editCategory');
    Route::match(['get', 'post'],'/admin/delete-category/{id}','CategoryController@deleteCategory');
    Route::get('/admin/view-categories', 'CategoryController@viewCategories');

    //Product Route
    Route::match(['get', 'post'], '/admin/add-product', 'ProductsController@addProduct');
    Route::match(['get', 'post'], '/admin/edit-product/{id}', 'ProductsController@editProduct');
    Route::get('/admin/view-products', 'ProductsController@viewProducts');
    Route::get('/admin/delete-product/{id}','ProductsController@deleteProduct');
    Route::get('/admin/delete-product-image/{id}', 'ProductsController@deleteProductImage');

    //Product Attribute Routes
    Route::match(['get','post'], '/admin/add-attributes/{id}','ProductsController@addAttributes');
    Route::match(['get','post'], '/admin/edit-attributes/{id}','ProductsController@editAttributes');
    Route::match(['get','post'], '/admin/add-images/{id}','ProductsController@addImages');
    Route::get('/admin/delete-attribute/{id}', 'ProductsController@deleteAttribute');
    Route::get('/admin/delete-product-image/{id}', 'ProductsController@deleteProductImage');
    Route::get('/admin/delete-alt-image/{id}', 'ProductsController@deleteAltImage');

    //Coupon Routes
    Route::match(['get', 'post'], '/admin/add-coupon', 'CouponsController@addCoupon');
    Route::get('/admin/view-coupons', 'CouponsController@viewCoupons');
    Route::match(['get', 'post'], '/admin/edit-coupon/{id}', 'CouponsController@editCoupon');
    Route::get('/admin/delete-coupon/{id}','CouponsController@deleteCoupon');

    //Admin Banners Route
    Route::match(['get', 'post'], '/admin/add-banner', 'BannersController@addBanner');
    Route::get('admin/view-banners','BannersController@viewBanners');
    Route::match(['get','post'],'/admin/edit-banner/{id}', 'BannersController@editBanner');
    Route::get('/admin/delete-banner/{id}', 'BannersController@deleteBanner');

    //Admin payment types
    Route::match(['get', 'post'], '/admin/add-payment-type', 'CheckoutController@addPaymentType');
    Route::get('/admin/view-payment-types', 'CheckoutController@viewPaymentTypes');
    Route::match(['get', 'post'], '/admin/edit-payment-type/{id}', 'CheckoutController@editPaymentType');
    Route::get('/admin/delete-payment-type-image/{id}', 'CheckoutController@deletePaymentTypeImage');

    //Admin order
    Route::get('/admin/view-orders', 'OrdersController@viewOrders');
    //Route::get('/admin/edit-order/', 'OrdersController@editOrders');
    Route::match(['get', 'post'], '/admin/edit-order/', 'OrdersController@editOrder');


    //Admin Contact Info
    Route::get('/admin/view-contact-info', 'ContactUsController@viewContactInfo');
    Route::match(['get', 'post'], '/admin/edit-contact-info/{id}', 'ContactUsController@editContactInfo');
    //Social Media
    Route::match(['get', 'post'], '/admin/add-contact-social-media', 'ContactUsController@addSocialMedia');
    Route::get('/admin/view-contact-social-media', 'ContactUsController@viewSocialMedia');
    Route::match(['get', 'post'], '/admin/edit-contact-social-media/{id}', 'ContactUsController@editSocialMedia');
    Route::get('/admin/delete-contact-social-media/{id}', 'ContactUsController@deleteSocialMedia');

    //Admin Page Settings
    Route::get('/admin/page-settings/view-page-logo', 'PageSettingsController@viewPageSettings');
    Route::match(['get','post'], '/admin/page-settings/edit-page-settings','PageSettingsController@editPageSettings');

    //Admin Footer Routes
    //Support and Services
    //How to place order
    Route::get('/admin/page-settings/view-how-to-place-order', 'PageSettingsController@viewHowToPlaceOrder');
    Route::match(['get','post'], '/admin/page-settings/edit-how-to-place-order','PageSettingsController@editHowToPlaceOrder');

    //FAQ
    Route::get('/admin/page-settings/view-faq', 'PageSettingsController@viewFAQ');
    Route::match(['get','post'], '/admin/page-settings/edit-faq','PageSettingsController@editFAQ');


    //Terms and condition
    //Terms of use
    Route::get('/admin/page-settings/view-terms-of-use', 'PageSettingsController@viewTermsOfUse');
    Route::match(['get','post'], '/admin/page-settings/edit-terms-of-use','PageSettingsController@editTermsOfUse');

    //Refund Policy
    Route::get('/admin/page-settings/view-refund-policy', 'PageSettingsController@viewRefundPolicy');
    Route::match(['get','post'], '/admin/page-settings/edit-refund-policy','PageSettingsController@editRefundPolicy');

    //About Company
    //Company Information
    Route::get('/admin/page-settings/view-company-information', 'PageSettingsController@viewCompanyInformation');
    Route::match(['get','post'], '/admin/page-settings/edit-company-information','PageSettingsController@editCompanyInformation');

    //Careers
    Route::get('/admin/view-careers', 'PageSettingsController@viewCareers');
    Route::match(['get','post'], '/admin/add-career/','PageSettingsController@addCareer');
    Route::match(['get','post'], '/admin/edit-career/{id}','PageSettingsController@editCareer');
    Route::get('/admin/delete-career/{id}','PageSettingsController@deleteCareer');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//terms_and_condition
Route::get('/terms-and-condition/payment-methods', 'PageSettingsController@paymentMethods');//payment methods
Route::get('/terms-and-condition/terms-of-use', 'PageSettingsController@termsOfUse');//Terms of use
Route::get('/terms-and-condition/refund-policy', 'PageSettingsController@refundPolicy');//Refund Policy

//About Company
Route::get('/about-company/company-information', 'PageSettingsController@companyInformation');//company information
Route::get('/about-company/careers', 'PageSettingsController@careers');//careers


//Support and Services
Route::get('/support-and-services/how-to-place-order', 'PageSettingsController@howToPlaceOrder');
Route::get('/support-and-services/faq', 'PageSettingsController@faq');

//Category listing page
Route::get('/products/{url}','ProductsController@products');

//Product Details Page
Route::get('product/{id}','ProductsController@product');

//Get Product Attribute Price
Route::get('/get-product-price', 'ProductsController@getProductPrice');

//Apply Coupon
Route::post('/cart/apply-coupon', 'ProductsController@applyCoupon');
Route::get('/cart/apply-coupon/{id}', 'ProductsController@applyCoupontoProduct');

//Add to Cart Rout
Route::match(['get', 'post'], '/add-cart', 'ProductsController@addtocart');

//Update cart after login
Route::match(['get', 'post'], '/update-cart', 'ProductsController@updateCart');



//Cart Route
Route::match(['get', 'post'], '/cart', 'ProductsController@cart');

//Delete Product from Cart Page
Route::get('/cart/delete-product/{id}', 'ProductsController@deleteCartProduct');

//Update Product Quantity in Cart
Route::get('/cart/update-quantity/{id}/{quantity}','ProductsController@updateCartQuantity');

//Track Order
Route::get('/track-orders', 'OrdersController@trackOrders')->name('orders.track');
Route::get('/track-orders/delete-order/{id}', 'OrdersController@deleteOrder');


//User login and register page
Route::get('/login-register', 'UsersController@userLoginRegister')->name('user.login');

//User Register Form Submit
Route::post('/user-register', 'UsersController@register')->name('user.register');

//User Login Form Submit
Route::post('/user-login', 'UsersController@login')->name('user.old.login');

//User logout
Route::get('/user-logout', 'UsersController@logout')->middleware('auth');



//All Routes Aftet login
//User Account Page
Route::match(['get','post'],'account','UsersController@account')->middleware('auth');
Route::get('/confirm-order/{url}','OrdersController@confirmOrder')->middleware('auth');;
//Checkout controller
Route::get('/checkout', 'CheckoutController@checkout')->middleware('auth');
//Current User Current Password
Route::post('/check-user-pwd', 'UsersController@chkUserPassword')->middleware('auth');

Route::match(['get','post'],'/update-user-pwd','UsersController@updatePassword');

//All Routes after login
/*Route::group(['middleware'=>['frontlogin']], function (){
    //User Account Page
    Route::match(['get','post'],'account','UsersController@account');
    Route::get('/confirm-order/{url}','OrdersController@confirmOrder');
    //Checkout controller
    Route::get('/checkout', 'CheckoutController@checkout');
    //Current User Current Password
    Route::post('/check-user-pwd', 'UsersController@chkUserPassword');
});*/

//Route::match(['get', 'post'], '/login-register', 'UsersController@register');

//Check if User already exists
Route::match(['get','post'],'/check-email','UsersController@checkEmail');

Route::get('/contact-us', 'ContactUsController@contactUs');

Route::post('/contactSend','ContactUsController@sendMail')->name('mail.send');

